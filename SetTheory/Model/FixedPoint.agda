
{-# OPTIONS --without-K --rewriting #-}
module SetTheory.Model.FixedPoint where
open import lib.Base
open import lib.Equivalences
open import lib.Equivalences2
open import lib.Funext
open import lib.NType
open import lib.NType2
open import lib.types.Empty
open import lib.types.Lift
open import lib.types.Nat hiding (_+_)
open import lib.types.Paths
open import lib.types.Pi
open import lib.types.Sigma
open import lib.types.Truncation
open import lib.types.Unit
open import lib.PathGroupoid
open import lib.PathOver
open import lib.Univalence
open import Notation
open import Smallness
open import Function.Embeddings
open import Function.Fiberwise
open import Function.ImageFactorisation
open import Propositions.Existensial
open import Propositions.Equivalences
open import Power
open import Propositions.Disjunction

module _ {i j} (V : Σ (Type j) (LocallySmall i))
     (sup : P i ¦ V ¦ → ¦ V ¦) (sup-equiv : is-equiv sup) where
  desup : ¦ V ¦ → P i ¦ V ¦
  desup = <– (sup , sup-equiv) 

  _∈_ : ¦ V ¦ → ¦ V ¦ → Type (lmax i j)
  x ∈ y = let (A , f) = desup y in
          Σ A (λ i → (f 〈 i 〉) == x)

  ∈-is-prop : ∀ {x y} → is-prop (x ∈ y)
  ∈-is-prop {x} {y} = snd (snd (desup y)) x

  postulate ==-is-prop : (x y : ¦ V ¦) → is-prop (x == y)

  set==-≃ : (x y : ¦ V ¦)
          → (x == y)
          ≃ Σ (fst (desup x) ≃ fst (desup y))
              (λ e → fst (snd (desup x)) == fst (snd (desup y)) ∘ –> e)
  set==-≃ x y = let (A , f) = desup x
                    (B , g) = desup y in

                          equiv-Σ (ua-equiv ⁻¹)
                                  (λ e → post∙-equiv
                                           (ap (λ α → fst (snd (desup y)) ∘ α)
                                               (λ= (coe-β e)))
                                         ∘e !-equiv)
                          ∘e (((T=-equiv ¦ V ¦)
                          ∘e (ap fst , hfiber-prop-ap≃
                                         (fst-embedding
                                           (λ _ → is-embedding-is-prop _)) _ _))
                          ∘e equiv-ap (P-is-T-subtype ¦ V ¦) _ _)
                          ∘e equiv-ap ((sup , sup-equiv) ⁻¹) _ _

  set==-≃-sup : ∀ A f y
              → (y == sup (A , f))
              ≃ Σ (fst (desup y) ≃ A)
                  (λ e → fst (snd (desup y)) == fst f ∘ –> e)
  set==-≃-sup A f y = coe-equiv (ap (λ Af → Σ (fst (desup y) ≃ fst Af)
                                              (λ e → fst (snd (desup y))
                                                     == fst (snd Af) ∘ –> e))
                                    (<–-inv-l (sup , sup-equiv) (A , f)))
                      ∘e set==-≃ y (sup (A , f))
          
  ∈-sup : ∀ {A f} → ∀ x
        → (x ∈ sup (A , f)) == (Σ A (λ i → (f 〈 i 〉) == x))
  ∈-sup {A} {f} x 
    = ap (λ Af → let (A' , f') = Af in
                 Σ A' (λ i → (f' 〈 i 〉) == x)) 
         (<–-inv-l (sup , sup-equiv) (A , f))

  _ε_ : ¦ V ¦ → ¦ V ¦ → Type i
  x ε y = let (A , f) = desup y in
          Σ A (λ i → (f 〈 i 〉) ≈ x ∷ V)

  _∉_ : ¦ V ¦ → ¦ V ¦ → Type (lmax i j)
  x ∉ y = ¬ (x ∈ y)

  set-index-type-eqv : ∀ x → Σ ¦ V ¦ (λ y → y ∈ x) ≃ fst (desup x)
  set-index-type-eqv x = let (A , f) = desup x in
                             equiv (λ (_ , yinx) → fst yinx)
                                   (λ a → (f 〈 a 〉) , (a , idp))
                                   (λ _ → idp)
                                   λ (y , yinx) → pair= (snd yinx)
                                                        (fst (is-prop-has-contr-path-over
                                                               (λ _ → ∈-is-prop)
                                                               _
                                                               (fst yinx , idp)
                                                               yinx))

  {- Proof of Axioms -}
  {- 
     The aim is to prove that fixed point models satisfy
     the following axioms:
      • Extensionality
      • Replacement
      • Restricted separation
      • Pairing
      • Union
      • Exponentiation
      • Natural numbers
  -}

{- Empty set -}
  ∅ : ¦ V ¦
  ∅ = sup (Lift ⊥ , ⊥-rec ∘ lower , λ{_ ()})
  
  empty-set : ∃ ¦ V ¦ (λ u → ∀ x → x ∉ u)
  empty-set = [ ∅ , (λ x → transport! ¬ (∈-sup x) (λ{()})) ]

  not-in-∅ : ∀ x → x ∉ ∅
  not-in-∅ x xin∅ = lower (transport fst (<–-inv-l (sup , sup-equiv) _) (fst xin∅))



{- Extensionality -}
  extensionality : ∀ x y → (∀ u → u ∈ x ↔ u ∈ y) → x == y
  extensionality x y α = let (A , f) = desup x in

                         <– (set==-≃ x y) (((set-index-type-eqv y
                                            ∘e equiv-Σ-snd
                                                 (λ u → <– (prop-≃-is-↔ ∈-is-prop
                                                                        ∈-is-prop)
                                                           (α u)))
                                            ∘e set-index-type-eqv x ⁻¹)
                                          , λ= (λ a → ! (snd (fst (α (f 〈 a 〉))
                                                                  (a , idp)))))

  ==-∅ : ∀ x → (∀ y → y ∉ x) → x == ∅
  ==-∅ x f = extensionality x ∅ (λ u → (λ uinx → ⊥-elim (f u uinx)) ,
                                        (λ uin∅ → ⊥-elim (not-in-∅ u uin∅)))



{- Pairing -}
  unordered-pair : ¦ V ¦ → ¦ V ¦ → ¦ V ¦
  unordered-pair x y = sup (Image {Domain = Lift {j = i} ⊤ + Lift {j = i} ⊤}
                                  (snd V)
                                  (cases (cst x) (cst y)) ,
                           image-inclusion _ _)

  syntax unordered-pair x y = ｛ x ,, y ｝

  ∈-unordered-pair : ∀ x y z
                   → z ∈ ｛ x ,, y ｝
                   ↔ (z == x) ∨ (z == y)
  ∈-unordered-pair x y z = transport! (λ A → A ↔ ((z == x) ∨ (z == y))) (∈-sup z)
                                      ((λ (u , p) → Trunc-rec Trunc-level
                                                              ([_] ∘ (λ { (left (lift tt) , s)
                                                                        → left (! p ∙ ! s);
                                                              (right (lift tt) , s) → right (! p ∙ ! s) }))
                                                              (image-complete _ _ u)) ,
                                      ∨-elim' (λ _ → snd (image-inclusion _ _) z)
                                              (λ p → ((image-quotient _ _) 〈 left (lift tt) 〉) , ! p)
                                              (λ p → ((image-quotient _ _) 〈 right (lift tt) 〉) , ! p))

  pairing : ∀ x y → ∃ ¦ V ¦ (λ u → ∀ z → z ∈ u ↔ (z == x) ∨ (z == y))
  pairing x y = [ ｛ x ,, y ｝ , ∈-unordered-pair x y ]



{- Union -}
  ⋃ : ¦ V ¦ → ¦ V ¦
  ⋃ x = let (A , f) = desup x in
        sup ((Image {Domain = Σ A (λ a → fst (desup (f 〈 a 〉)))}
                    (snd V)
                    (λ (a , b) → snd (desup (f 〈 a 〉)) 〈 b 〉 )) ,
            image-inclusion _ _)

  trsp2 : ∀ {i j k} {A : Type i} {B : A → Type j} (P : (a : A) → B a → Type k)
      → {a a' : A}
      → (p : a == a')
      → (b : B a)
      → P a b → P a' (transport B p b)
  trsp2 P idp b p = p

  ∈-⋃ : ∀ x y
      → (y ∈ ⋃ x)
      ↔ ∃ ¦ V ¦ (λ z → (y ∈ z) × (z ∈ x))
  ∈-⋃ x y = transport! (λ A → A ↔ ∃ ¦ V ¦ (λ z → (y ∈ z) × (z ∈ x))) (∈-sup y)
                       ((λ (u , p) → Trunc-rec Trunc-level
                                               ([_] ∘ (λ ((a , a') , q) → (snd (desup x) 〈 a 〉) ,
                                                                          ((a' , (q ∙ p)) , (a , idp))))
                                               (image-complete _ _ u)) ,
                       ∃-elim _ _
                              (λ _ → snd (image-inclusion _ _) y)
                              λ (z , ((c , q) , (a , p)))
                                → (image-quotient _ _) 〈 a , transport (λ w → fst (desup w)) (! p) c 〉  ,
                                  trsp2 (λ w t → snd (desup w) 〈 t 〉 == y) (! p) c q)

  union : ∀ x → ∃ ¦ V ¦ (λ u → ∀ y → y ∈ u ↔ ∃ ¦ V ¦ (λ z → (y ∈ z) × (z ∈ x)))
  union x = [ (⋃ x) , ∈-⋃ x ]

  ∈-bin-union : ∀ x y z → (z ∈ ⋃ ｛ x ,, y ｝) ↔ ((z ∈ x) ∨ (z ∈ y))
  ∈-bin-union x y z = (((∃-elim _ _
                                (λ _ → ∨-is-prop _ _)
                                λ (u , (zinu , uinxy))
                                  → ∨-elim' (λ _ → ∨-is-prop _ _)
                                            (λ ueqx → [ left (transport (λ A → z ∈ A) ueqx zinu) ])
                                            (λ ueqy → [ right (transport (λ A → z ∈ A) ueqy zinu) ])
                                    (fst (∈-unordered-pair x y u) uinxy)) ,
                        ∨-elim' (λ _ → ∃-is-prop _ _)
                                (λ zinx → [ x , (zinx , snd (∈-unordered-pair x y x) [ left idp ]) ])
                                (λ ziny → [ y , (ziny , snd (∈-unordered-pair x y y) [ right idp ]) ]))
                      ∘↔ ∈-⋃ _ _)



{- Natural numbers -}
  singleton : ¦ V ¦ → ¦ V ¦
  singleton x = sup ((Lift {j = i} ⊤)
                    , ((λ tt → x)
                    , λ y → Σ-level (Lift-level Unit-is-prop)
                                    λ _ → ==-is-prop x y))

  syntax singleton x = ｛ x ｝

  ∈-singleton : ∀ x y
               → y ∈ ｛ x ｝
               ↔ y == x
  ∈-singleton x y = (λ (a , p) → ! p ∙ idp) ∘ coe (∈-sup y) ,
                    λ yeqx → transport! (λ A → A ∈ singleton x) yeqx
                                        (coe! (∈-sup x) (lift tt , idp))

  data Accessible (x : ¦ V ¦) : Type (lmax i j) where
    accessible : ((y : ¦ V ¦) → y ∈ x → Accessible y) → Accessible x

  Accessible-is-prop : ∀ x
                     → is-prop (Accessible x)
  Accessible-is-prop x = all-paths-is-prop λ s t → Acc-all-paths x s t where

                           Acc-all-paths : ∀ x
                                         → (s t : Accessible x)
                                         → s == t
                           Acc-all-paths x (accessible f) (accessible g)
                                         = ap accessible (λ= (λ y → λ= (λ yinx → Acc-all-paths
                                                                                   y
                                                                                   (f y yinx)
                                                                                   (g y yinx))))

  Accessible-ind : ∀ {i} {P : ¦ V ¦ → Type i}
                  → (∀ x → Accessible x → (∀ y → y ∈ x → P y) → P x)
                  → ∀ x → Accessible x → P x
  Accessible-ind H x (accessible f)
                 = H x (accessible f) (λ y yinx → Accessible-ind H y (f y yinx))

  acc-x∉x : ∀ x → Accessible x → x ∉ x
  acc-x∉x = Accessible-ind λ x accx H xinx → H x xinx xinx

  acc-x∉⋃x : ∀ x → Accessible x → ¬ (Σ ¦ V ¦ (λ y → (x ∈ y) × (y ∈ x)))
  acc-x∉⋃x = Accessible-ind (λ x accx H (y , (xiny , yinx)) → H y yinx (x , (yinx , xiny)))

  set-S : ¦ V ¦ → ¦ V ¦
  set-S x = ⋃ ｛ x ,, ｛ x ｝ ｝

  abstract

    ∈-set-S : ∀ {x y} → (y ∈ set-S x) ↔ ((y ∈ x) ∨ (y == x))
    ∈-set-S {x} {y} = (((Trunc-rec Trunc-level
                                 (cases (λ yinx → [ left yinx ])
                                        (λ yin｛x｝ → [ right (fst (∈-singleton x y) yin｛x｝) ]))) ,
                      Trunc-rec Trunc-level
                                (cases (λ yinx → [ left yinx ])
                                       (λ yeqx → [ right (snd (∈-singleton x y) yeqx) ])))
                    ∘↔ ∈-bin-union _ _ _)

  x-∈-set-S-x : ∀ x
              → x ∈ set-S x
  x-∈-set-S-x x = snd ∈-set-S [ right idp ]

  acc-set-S-is-inj : ∀ {x y} → Accessible x → Accessible y → set-S x == set-S y → x == y
  acc-set-S-is-inj {x} {y} accx accy p
                   = ∨-elim' (λ _ → ==-is-prop _ _)
                             (λ xiny → ∨-elim' (λ _ → ==-is-prop _ _)
                                               (λ yinx → ⊥-elim (acc-x∉⋃x x
                                                                           accx
                                                                           (y , (xiny , yinx))))
                                               (λ q → ! q)
                                               (fst ∈-set-S (transport! (λ X → y ∈ X)
                                                                         p
                                                                         (x-∈-set-S-x y))))
                             (idf _)
                             (fst ∈-set-S (transport (λ X → x ∈ X) p (x-∈-set-S-x x)))

  nat-fun : Lift {j = i} ℕ → ¦ V ¦
  nat-fun (lift O) = ∅
  nat-fun (lift (S n)) = set-S (nat-fun (lift n))

  elems-are-acc : ∀ {x} → Accessible x → (y : ¦ V ¦) → y ∈ x → Accessible y
  elems-are-acc {x} (accessible f) y yinx = f y yinx

  acc-nat-fun : ∀ n → Accessible (nat-fun n)
  acc-nat-fun (lift O) = accessible (curry (((⊥-elim
                                    ∘ –> lift-equiv)
                                    ∘ coe (ap fst (<–-inv-l (sup , sup-equiv) _)))
                                    ∘ –> (set-index-type-eqv _)))
  acc-nat-fun (lift (S n)) = accessible (λ y yinS → ∨-elim' (λ _ → Accessible-is-prop y)
                                                     (λ yinn → elems-are-acc
                                                                 (acc-nat-fun (lift n))
                                                                 y
                                                                 yinn)
                                                     (λ p → transport!
                                                              (λ X → Accessible X)
                                                              p
                                                              (acc-nat-fun (lift n)))
                                                     (fst ∈-set-S yinS))

  set-S-non-empty : ∀ n → Σ ¦ V ¦ (λ u → u ∈ set-S (nat-fun n))
  set-S-non-empty (lift O) = ∅ , (snd ∈-set-S [ right idp ])
  set-S-non-empty (lift (S n)) = (fst (set-S-non-empty (lift n))) ,
                          (snd ∈-set-S [ left (snd (set-S-non-empty (lift n))) ])

  nat-fun-Sn-neq-∅ : ∀ n → set-S (nat-fun n) ≠ ∅
  nat-fun-Sn-neq-∅ n eq∅ = not-in-∅ (fst (set-S-non-empty n))
                                        (transport (λ X → fst (set-S-non-empty n) ∈ X)
                                                   eq∅
                                                   (snd (set-S-non-empty n)))

  nat-fun-is-inj : ∀ m n → nat-fun m == nat-fun n → m == n
  nat-fun-is-inj (lift O) (lift O) p = idp
  nat-fun-is-inj (lift O) (lift (S n)) p = ⊥-elim (nat-fun-Sn-neq-∅ (lift n) (! p))
  nat-fun-is-inj (lift (S m)) (lift O) p = ⊥-elim (nat-fun-Sn-neq-∅ (lift m) p)
  nat-fun-is-inj (lift (S m)) (lift (S n)) p = ap (lift ∘ S ∘ lower) (nat-fun-is-inj
                                        (lift m) (lift n) (acc-set-S-is-inj
                                              (acc-nat-fun (lift m))
                                              (acc-nat-fun (lift n))
                                              p))

  nat-fun-is-emb : is-embedding nat-fun
  nat-fun-is-emb = inj-to-emb ==-is-prop
                              (nat-fun-is-inj _ _)

  nat-set : ¦ V ¦
  nat-set = sup ((Lift ℕ) , nat-fun , nat-fun-is-emb)

  ∅-∈-nat-set : ∅ ∈ nat-set
  ∅-∈-nat-set = coe! (∈-sup ∅) ((lift 0) , idp)

  set-S-∈-nat-set : ∀ x → x ∈ nat-set → set-S x ∈ nat-set
  set-S-∈-nat-set x xinnat = coe! (∈-sup (set-S x))
                                  (lift (S (lower (fst (coe (∈-sup x) xinnat)))) ,
                                   ap set-S (snd (coe (∈-sup x) xinnat)))

  ∅-or-S-set : ∀ x → x ∈ nat-set
             ↔ (x == ∅) + (Σ ¦ V ¦ (λ y → (y ∈ nat-set) × (x == set-S y)))
  ∅-or-S-set x = (transport!
                    (λ X → X → (x == ∅) + Σ ¦ V ¦ (λ y → (y ∈ nat-set) × (x == set-S y)))
                    (∈-sup x)
                    (ϕ x) ,
                  ψ x) where

                     ϕ : ∀ x
                       → Σ (Lift ℕ) (λ n → nat-fun n == x)
                       → (x == ∅) + (Σ ¦ V ¦ (λ y → (y ∈ nat-set) × (x == set-S y)))
                     ϕ x (lift O , p) = left (! p)
                     ϕ x (lift (S n) , p) = right (nat-fun (lift n) ,
                                                   (coe! (∈-sup _) (lift n , idp)) ,
                                                   ! p)

                     ψ : ∀ x
                       → (x == ∅) + (Σ ¦ V ¦ (λ y → (y ∈ nat-set) × (x == set-S y)))
                       → x ∈ nat-set
                     ψ x = cases (λ p → transport! (λ X → X ∈ nat-set) p ∅-∈-nat-set)
                                 (λ (y , yinnat , q)
                                   → transport! (λ X → X ∈ nat-set)
                                                q
                                                (set-S-∈-nat-set y yinnat))

  natural-numbers : ∃ ¦ V ¦ (λ x → ∀ u → (u ∈ x)
                                         ↔ (u == ∅
                                           ∨ ∃ ¦ V ¦ (λ v → (v ∈ x)
                                                             × (u == set-S v))))
  natural-numbers = [ nat-set , (λ u → (ϕ u) , (ψ u)) ] where

                         ϕ : ∀ x
                           → x ∈ nat-set
                           → (x == ∅) ∨ ∃ ¦ V ¦ (λ y → (y ∈ nat-set) × (x == set-S y))
                         ϕ x = cases (λ p → [ left p ])
                                     (λ q → [ right [ q ] ])
                               ∘ fst (∅-or-S-set x)

                         ψ : ∀ x
                           → (x == ∅) ∨ ∃ ¦ V ¦ (λ y → (y ∈ nat-set) × (x == set-S y))
                           → x ∈ nat-set
                         ψ x = ∨-elim' (λ _ → ∈-is-prop)
                                       (λ p → snd (∅-or-S-set x) (left p))
                                       (∃-elim _ _
                                               (λ _ → ∈-is-prop)
                                               (λ q → snd (∅-or-S-set x) (right q)))

  nat-set-induction : ∀ {i} {P : ¦ V ¦ → Type i}
                    → P ∅
                    → (∀ x → x ∈ nat-set → P x → P (set-S x))
                    → ∀ x → x ∈ nat-set → P x
  nat-set-induction {P = P} P∅ IH = Π-on-¦V¦ (ϕ {P = P}
                                                 P∅
                                                 (λ n → IH
                                                        (nat-fun n)
                                                        (coe! (∈-sup (nat-fun n))
                                                              (n , idp)))) where

                    Π-on-¦V¦ : ∀ {k} {P : ¦ V ¦ → Type k} {A : Type i} {f : A ↪ ¦ V ¦}
                             → (∀ a → P (f 〈 a 〉))
                             → ∀ y → y ∈ sup (A , f) → P y
                    Π-on-¦V¦ {P = P} H y yinsup
                             = transport! P (! (snd (coe (∈-sup y) yinsup))) (H _)

                    ϕ : ∀ {i} {P : ¦ V ¦ → Type i}
                      → P ∅
                      → (∀ n → P (nat-fun n) → P (set-S (nat-fun n)))
                      → ∀ n → P (nat-fun n)
                    ϕ {P = P} P∅ IH (lift O) = P∅
                    ϕ {P = P} P∅ IH (lift (S n))
                      = IH (lift n) (ϕ {P = P} P∅ IH (lift n))



{- Restricted separation -}
  separate : ¦ V ¦ → (¦ V ¦ → hProp i) → ¦ V ¦
  separate x C = let (A , f) = desup x in
                     sup (Σ A (¦_¦ ∘ C ∘ (fst f)) ,
                         (fst , (fst-embedding (λ a → snd (C (f 〈 a 〉)))) ∘emb f))

  syntax separate x (λ z → C) = [ z ∈ x ¦ C ]

  restricted-separation : (C : ¦ V ¦ → hProp i) → ∀ x → ∃ ¦ V ¦ (λ u → ∀ z → z ∈ u ↔ ¦ C z ¦ × (z ∈ x))
  restricted-separation C x = [ [ z ∈ x ¦ C z ] , ϕ ] where
                                ϕ : ∀ z
                                  → z ∈ [ z ∈ x ¦ C z ]
                                  ↔ ¦ C z ¦ × (z ∈ x)
                                ϕ z = transport! (λ A → A ↔ ¦ C z ¦ × (z ∈ x)) (∈-sup z)
                                                 ((λ ((a , c) , p) → (transport (¦_¦ ∘ C) p c) , (a , p)) ,
                                                 λ (c , (a , p)) → (a , (transport! (¦_¦ ∘ C) p c)) , p)



{- Replacement -}
  image-set : ∀ {i} → (C : ¦ V ¦ → ¦ V ¦ → Type i)
            → ∀ u → (∀ x → x ∈ u → ∃! ¦ V ¦ (λ y → C x y))
            → ¦ V ¦
  image-set C u r = let (A , f) = desup u in
                         sup (Image (snd V)
                                    (λ a → fst (fst (r (f 〈 a 〉) (a , idp)))) ,
                             image-inclusion _ _)

  replacement : ∀ {i} → (C : ¦ V ¦ → ¦ V ¦ → Type i)
              → ∀ u → (∀ x → x ∈ u → ∃! ¦ V ¦ (λ y → C x y))
              → ∃ ¦ V ¦ (λ v → ∀ y → y ∈ v ↔ ∃ ¦ V ¦ (λ x → (x ∈ u) × C x y))
  replacement C u r
    = [ (image-set C u r) , ϕ ] where

        f : Image (snd V) (λ a → fst (fst (r _ _)))
          → ¦ V ¦
        f a = image-inclusion _ _ 〈 a 〉

        ψ : ∀ y
          → hfiber f y
          → ∃ ¦ V ¦ (λ x → (x ∈ u) × C x y)
        ψ y (z , p) = Trunc-rec Trunc-level
                                ([_] ∘ λ (a , q) → (snd (desup u) 〈 a 〉) ,
                                                   ((a , idp) ,
                                                   transport (C (snd (desup u) 〈 a 〉))
                                                             (q ∙ p)
                                                             (snd (fst (r _ _)))))
                                (image-complete _ _ z)

        θ : ∀ y
          → Σ ¦ V ¦ (λ x → (x ∈ u) × C x y)
          → hfiber f y
        θ y (x , ((a , p) , c)) = (image-quotient _ _ 〈 a 〉) ,
                                  ap fst (contr-has-all-paths
                                           (r (snd (desup u) 〈 a 〉) (a , idp))
                                           (fst (r _ _))
                                           (y , transport! (λ w → C w y) p c))

        ϕ : ∀ y
          → (y ∈ image-set C u r)
          ↔ ∃ ¦ V ¦ (λ x → (x ∈ u) × C x y)
        ϕ y = transport! (λ A → A ↔ ∃ ¦ V ¦ (λ x → (x ∈ u) × C x y)) (∈-sup y)
                         (ψ y ,
                         ∃-elim _ _
                                (λ _ → snd (image-inclusion _ _) y)
                                (θ y))



{- Exponentiation -}
  module _ (ordered-pair : ¦ V ¦ × ¦ V ¦ ↪ ¦ V ¦) where

    graph-index-types-emb : ∀ {i j} {A : Type i} {B : Type j}
                          → is-set B
                          → (A → B)
                          → A ↪ (A × B)
    graph-index-types-emb s f = (λ a → (a , f a)) ,
                                λ (a , b) → equiv-preserves-level
                                              (equiv-Σ-snd (λ a → =×-eqv ⁻¹) ∘e Σ-assoc)
                                              (Σ-level (contr-is-prop (pathto-is-contr a))
                                                       (λ u → s (f (fst u)) b))

    graph-emb : ∀ {i j} {A : Type i} {B : Type j}
              → A ↪ ¦ V ¦
              → B ↪ ¦ V ¦
              → (A → B)
              → A ↪ ¦ V ¦
    graph-emb f g σ = (graph-index-types-emb (embedding-preserves-level g ==-is-prop) σ
                      ∘emb (f ×emb g))
                      ∘emb ordered-pair

    graph-set : {A B : Type i}
              → A ↪ ¦ V ¦
              → B ↪ ¦ V ¦
              → (A → B) → ¦ V ¦
    graph-set {A} f g σ = sup (A , graph-emb f g σ)

    pathover-to-coe : ∀ {i j} {A B : Type i} {X : Type j}
                    → {f : A → X} {g : B → X}
                    → {α : A == B}
                    → f == g [ (λ Y → Y → X) ↓ α ]
                    → f == g ∘ coe α
    pathover-to-coe {α = idp} p = p

    graph-set-is-inj : {A B : Type i}
                     → (f : A ↪ ¦ V ¦)
                     → (g : B ↪ ¦ V ¦)
                     → (σ σ' : A → B)
                     → graph-set f g σ == graph-set f g σ'
                     → σ == σ'
    graph-set-is-inj {A} {B} f g σ σ' p
                     = λ= (λ a → snd×= (pairs-equal a)
                                 ∙ ap σ' (! (fst= (pairs-equal a)))) where

                          pairs-equal = λ a → emb-to-inj (snd ((f ×emb g)
                                                              ∘emb ordered-pair))
                                                         (app= (pathover-to-coe
                                                                 (↓-Σ-fst
                                                                   (snd=
                                                                     (equiv-inj
                                                                       (sup , sup-equiv)
                                                                       p))))
                                                               a)
    
    graph-set-is-emb : {A B : Type i}
                     → (f : A ↪ ¦ V ¦)
                     → (g : B ↪ ¦ V ¦)
                     → is-embedding (graph-set f g)
    graph-set-is-emb f g = inj-to-emb
                             ==-is-prop
                             λ {x} {y} → graph-set-is-inj f g x y

    exp-emb : {A B : Type i}
            → A ↪ ¦ V ¦
            → B ↪ ¦ V ¦
            → (A → B) ↪ ¦ V ¦
    exp-emb f g = (graph-set f g , graph-set-is-emb f g)

    exp-set : ¦ V ¦ → ¦ V ¦ → ¦ V ¦
    exp-set x y = sup ((fst (desup x) → fst (desup y))
                      , exp-emb (snd (desup x)) (snd (desup y)))

    is-total-fun : ¦ V ¦ → ¦ V ¦ → ¦ V ¦ → Type (lmax i j)
    is-total-fun z x y = (∀ u → u ∈ z → ∃ (¦ V ¦ × ¦ V ¦)
                                          (λ (v , w) → (v ∈ x)
                                                       × (w ∈ y)
                                                       × (u == ordered-pair 〈 v , w 〉)))
                         × (∀ v → v ∈ x → ∃! ¦ V ¦ (λ w → (ordered-pair 〈 v , w 〉 ∈ z)))

    syntax is-total-fun z x y = z :: x ⟶ y

    exponentiation : ∀ x y → ∃ ¦ V ¦ (λ u → ∀ z → z ∈ u ↔ z :: x ⟶ y)
    exponentiation x y = [ (exp-set x y) , (λ z → (–> (all-equivs z))
                                           , (<– (all-equivs z))) ] where

      A = fst (desup x)
      f = snd (desup x)
      B = fst (desup y)
      g = snd (desup y)

      ∃-v-w-is-prop : ∀ {x y u}
                    → is-prop (Σ (¦ V ¦ × ¦ V ¦)
                              (λ (v , w) → (v ∈ x) × (w ∈ y) × (u == ordered-pair 〈 v , w 〉)))
      ∃-v-w-is-prop = all-paths-is-prop
                        λ (_ , (_ , (_ , ueqvw)))
                          (_ , (_ , (_ , ueqv'w')))
                        → pair= (emb-to-inj (snd ordered-pair)
                                            (! ueqvw ∙ ueqv'w'))
                                (fst (is-prop-has-contr-path-over
                                        (λ _ → Σ-level ∈-is-prop
                                        (λ _ → Σ-level ∈-is-prop
                                        λ _ → ==-is-prop _ _))
                                        _ _ _))

      eqv-1 : ∀ z
            → (z :: x ⟶ y)
            ≃ (((u , _) : Σ ¦ V ¦ (λ u → u ∈ z))
              → Σ ((Σ ¦ V ¦ (λ v → v ∈ x)) × (Σ ¦ V ¦ (λ w → w ∈ y)))
                  (λ ((v , _) , (w , _)) → (u == ordered-pair 〈 v , w 〉)))
            × (((v , _) : Σ ¦ V ¦ (λ v → v ∈ x))
              → ∃! (Σ ¦ V ¦ (λ w → w ∈ y))
                   (λ (w , _) → (ordered-pair 〈 v , w 〉 ∈ z)))
      eqv-1 z = equiv-Σ (equiv-Π-r (λ (_ , _) → ((equiv-Σ (Σ-assoc
                                                          ∘e equiv-Σ Σ₂-×-comm
                                                                     λ _ → ide _)
                                                          (λ _ → ide _)
                                                ∘e Σ-assoc ⁻¹)
                                                ∘e Σ-assoc ⁻¹)
                                                ∘e unTrunc-equiv _ ∃-v-w-is-prop)
                        ∘e curry-equiv ⁻¹)
                        λ Γ → equiv-Π-r (λ (v , vinx) → <– (prop-≃-is-↔ is-contr-is-prop
                                                                          is-contr-is-prop)
                                                           (forward z Γ (v , vinx)
                                                           , backward z Γ (v , vinx)))
                              ∘e curry-equiv ⁻¹ where

            forward : ∀ z
                    → (Γ : ((u , _) : Σ ¦ V ¦ (λ u → u ∈ z))
                         → Σ ((Σ ¦ V ¦ (λ v → v ∈ x)) × (Σ ¦ V ¦ (λ w → w ∈ y)))
                             (λ ((v , _) , (w , _)) → u == ordered-pair 〈 v , w 〉))
                    → ((v , _) : Σ ¦ V ¦ (λ v → v ∈ x))
                    → ∃! ¦ V ¦ (λ w → (ordered-pair 〈 v , w 〉) ∈ z)
                    → ∃! (Σ ¦ V ¦ (λ w → w ∈ y))
                         (λ (w , _) → (ordered-pair 〈 v , w 〉) ∈ z)
            forward z Γ _ ((w , inz) , q)
                    = inhab-prop-is-contr
                        ((w
                        , (transport! (λ u → u ∈ y)
                                      (snd×= (emb-to-inj
                                             (snd ordered-pair)
                                             (snd (Γ (_ , inz)))))
                                      (snd (snd (fst (Γ (_ , inz)))))))
                        , inz)
                        (all-paths-is-prop
                           λ ((w' , _) , inz') ((w'' , _) , inz'')
                           → prop-equal (λ _ → ∈-is-prop)
                                        (prop-equal (λ _ → ∈-is-prop)
                                                    (fst= (! (q (w' , inz'))
                                                          ∙ q (w'' , inz'')))))

            backward : ∀ z
                     → (Γ : ((u , _) : Σ ¦ V ¦ (λ u → u ∈ z))
                          → Σ ((Σ ¦ V ¦ (λ v → v ∈ x)) × (Σ ¦ V ¦ (λ w → w ∈ y)))
                              (λ ((v , _) , (w , _)) → u == ordered-pair 〈 v , w 〉))
                     → ((v , _) : Σ ¦ V ¦ (λ v → v ∈ x))
                     → ∃! (Σ ¦ V ¦ (λ w → w ∈ y))
                          (λ (w , _) → (ordered-pair 〈 v , w 〉) ∈ z)
                     → ∃! ¦ V ¦ (λ w → (ordered-pair 〈 v , w 〉) ∈ z)
            backward z Γ _ (((w , winy) , inz) , q)
                     = inhab-prop-is-contr
                         (w , inz)
                         (all-paths-is-prop
                            (λ (w' , inz') (w'' , inz'')
                            → prop-equal (λ _ → ∈-is-prop)
                                         (fst= (fst= (contr-has-all-paths
                                                        (((w , winy) , inz) , q)
                                                        ((w'
                                                        , transport! (λ u → u ∈ y)
                                                                     (snd×= (emb-to-inj
                                                                            (snd ordered-pair)
                                                                            (snd (Γ (_ , inz')))))
                                                                     (snd (snd (fst (Γ (_ , inz'))))))
                                                        , inz')
                                                        ((w''
                                                        , transport! (λ u → u ∈ y)
                                                                     (snd×= (emb-to-inj
                                                                            (snd ordered-pair)
                                                                            (snd (Γ (_ , inz'')))))
                                                                     (snd (snd (fst (Γ (_ , inz''))))))
                                                        , inz''))))))

      eqv-2 : ∀ z
            → (((u , _) : Σ ¦ V ¦ (λ u → u ∈ z))
              → Σ ((Σ ¦ V ¦ (λ v → v ∈ x)) × (Σ ¦ V ¦ (λ w → w ∈ y)))
                  (λ ((v , _) , (w , _)) → (u == ordered-pair 〈 v , w 〉)))
            × (((v , _) : Σ ¦ V ¦ (λ v → v ∈ x))
              → ∃! (Σ ¦ V ¦ (λ w → w ∈ y))
                   (λ (w , _) → (ordered-pair 〈 v , w 〉 ∈ z)))
            ≃ ((c : fst (desup z))
              → Σ (A × B) (λ (a , b) → snd (desup z) 〈 c 〉
                                       == ordered-pair 〈 f 〈 a 〉 , g 〈 b 〉 〉))
            × ((a : A) → ∃! B (λ b → ordered-pair 〈 f 〈 a 〉 , g 〈 b 〉 〉 ∈ z))

      eqv-2 z = equiv-Σ (equiv-Π (set-index-type-eqv z)
                                 λ _ → equiv-Σ (equiv-Σ (set-index-type-eqv x)
                                                        (λ _ → set-index-type-eqv y))
                                               (λ _ → ide _))
                        λ _ → equiv-Π (set-index-type-eqv x)
                                      λ a → is-contr-eqv
                                               (equiv-Σ (set-index-type-eqv y)
                                                        (λ _ → ide _))

      eqv-3 : ∀ z
            → ((c : fst (desup z))
              → Σ (A × B) (λ (a , b) → snd (desup z) 〈 c 〉
                                       == ordered-pair 〈 f 〈 a 〉 , g 〈 b 〉 〉))
            × ((a : A) → ∃! B (λ b → ordered-pair 〈 f 〈 a 〉 , g 〈 b 〉 〉 ∈ z))
            ≃ Σ (Σ (fst (desup z) → A)
                   (λ α → (c : fst (desup z))
                          → Σ B (λ b → snd (desup z) 〈 c 〉
                                       == ordered-pair 〈 f 〈 α c 〉 , g 〈 b 〉 〉)))
                (λ (e , P) → (a : A) → is-contr (hfiber e a))
      eqv-3 z = (equiv-Σ (ide _)
                         λ (e , P)
                         → equiv-Π-r
                             (λ a → is-contr-eqv
                                      (equiv-Σ-snd
                                        (λ c → <– (prop-≃-is-↔
                                                    (all-paths-is-prop
                                                      (λ (b , p) (b' , p')
                                                      → prop-equal (λ _ → ==-is-prop _ _)
                                                                   (snd×= (emb-to-inj
                                                                            (snd ((f ×emb g)
                                                                                 ∘emb ordered-pair))
                                                                            (! p ∙ p')))))
                                                    (embedding-preserves-level
                                                      f ==-is-prop _ _))
                                                  ((λ (b , p) → fst= (emb-to-inj
                                                                       (snd ((f ×emb g)
                                                                            ∘emb ordered-pair))
                                                                       (! (snd (P c)) ∙ p)))
                                                  , λ p → transport
                                                            (λ X → Σ B
                                                                     (λ b → snd (desup z) 〈 c 〉
                                                                            == ordered-pair
                                                                                 〈 f 〈 X 〉
                                                                                 , g 〈 b 〉 〉))
                                                            p
                                                            (P c)))
                                      ∘e Σ₁-×-comm)))
                ∘e equiv-Σ (choice
                           ∘e equiv-Π (ide _)
                                      (λ _ → Σ-assoc))
                           λ _ → ide _

      eqv-4 : ∀ z
            → Σ (Σ (fst (desup z) → A)
                   (λ α → (c : fst (desup z))
                          → Σ B (λ b → snd (desup z) 〈 c 〉
                                       == ordered-pair 〈 f 〈 α c 〉 , g 〈 b 〉 〉)))
                (λ (e , P) → (a : A) → is-contr (hfiber e a))
            ≃ Σ (A → B)
                (λ σ → exp-emb (snd (is-equiv.g sup-equiv x))
                               (snd (is-equiv.g sup-equiv y)) 〈 σ 〉 == z)
      eqv-4 z = (equiv-Σ-snd (λ σ → !-equiv
                                    ∘e (set==-≃-sup _ _ _) ⁻¹
                                    ∘e equiv-Σ-snd (λ _ → λ=-equiv))
                ∘e Σ₁-×-comm)
                ∘e (equiv-Σ-snd (λ e → equiv-Σ (pre∘-equiv (e ⁻¹))
                                            (λ _ → ide _))
                ∘e equiv-Σ-snd λ e → choice)
                ∘e equiv-Σ (equiv-Σ-snd
                             (λ _ → <– (prop-≃-is-↔
                                         (Π-level (λ _ → is-contr-is-prop))
                                         (is-equiv-is-prop _))
                                       (contr-map-is-equiv
                                       , equiv-is-contr-map)))
                           (λ e → ide _)
                ∘e Σ₂-×-comm

      all-equivs : ∀ z
                 → (z ∈ exp-set x y)
                 ≃ (z :: x ⟶ y)
      all-equivs z = (coe-equiv (∈-sup z) ⁻¹
                     ∘e eqv-4 z
                     ∘e eqv-3 z
                     ∘e eqv-2 z
                     ∘e eqv-1 z) ⁻¹

