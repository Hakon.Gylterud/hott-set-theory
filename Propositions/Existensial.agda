{-# OPTIONS --without-K #-}
{-
% Truncated Existential Quantifier
% Håkon Robbestad Gylterud

This module implements the truncated existensial quantifier, using
the HoTT library truncations.
-}

module Propositions.Existensial where
open import lib.Base
open import lib.NType
open import lib.NType2

open import lib.types.Truncation
open import lib.types.Sigma
open import lib.types.Pi
open import lib.types.TLevel


module _ {i j} (A : Type i) (B : A → Type j) where

  ∃ : Type (lmax i j)
  ∃ = Trunc ⟨–1⟩ (Σ A B)

  ∃-elim : ∀ {j} {P : ∃ → Type j}
         → (p : (x : ∃) → is-prop (P x))
         → (d : (a : Σ A B) → P [ a ])
         → Π ∃ P
  ∃-elim p = Trunc-elim p

  ∃-is-prop : is-prop ∃
  ∃-is-prop = Trunc-level

  ∃! : Type (lmax i j)
  ∃! = is-contr (Σ A B)

  ∃!-prop : is-prop ∃!
  ∃!-prop = is-contr-is-prop

