{-# OPTIONS --without-K #-}
{-
 Binary disjunction
 Håkon Robbestad Gylterud

This module implements both disjoint union and truncated binary disjunction.
We use the HoTT library truncations.

-}


module Propositions.Disjunction where
open import lib.Base
open import lib.NType

open import lib.types.Truncation
open import lib.types.Sigma
open import lib.types.Pi
open import lib.types.TLevel


module _ {i j} where

  data _+_ (A : Type i) (B : Type j) : Type (lmax i j) where
     left : A → A + B
     right : B → A + B

  cases : ∀ {k} {A : Type i} {B : Type j}
        →  {C : A + B → Type k}
        → ((a : A) → C (left a))
        → ((b : B) → C (right b))
        → (x : A + B) → C x
  cases f g (left a) = f a
  cases f g (right b) = g b

  
  _∨_ : (A : Type i) (B : Type j) → Type (lmax i j)
  A ∨ B = Trunc ⟨–1⟩ (A + B)

  ∨-elim : ∀ {k} {A : Type i} {B : Type j} {P : A ∨ B → Type k}
         → (p : (x : A ∨ B) → is-prop (P x)) (d : (a : A + B) → P [ a ])
         → Π (A ∨ B) P
  ∨-elim p d = Trunc-elim p d

  ∨-elim' : ∀ {k} {A : Type i} {B : Type j} {P : A ∨ B → Type k}
         → (p : (x : A ∨ B) → is-prop (P x)) 
         → (d : (a : A) → P [ left a ])
         → (d' : (b : B) → P [ right b ])
         → Π (A ∨ B) P
  ∨-elim' p d d' = Trunc-elim p (cases d d')

  ∨-is-prop : (A : Type i) (B : Type j) → is-prop (A ∨ B)
  ∨-is-prop A B = Trunc-level

