{-# OPTIONS --without-K #-}

module Smallness where

open import lib.Base
open import lib.Equivalences

open import Function.Embeddings
open import Notation

Small : ∀ i {j} → Type j → Type (lmax (lsucc i) j)
Small i A = Σ (Type i) (_≃_ A)

LocallySmall : ∀ i {j} → Type j → Type (lmax (lsucc i) j)
LocallySmall i A = (x y : A) → Small i (x == y)

Equal : ∀ {i j} (A : Σ (Type j) (LocallySmall i)) → fst A → fst A → Type i
Equal A x y = ¦ (snd A x y) ¦

syntax Equal A x y = x ≈ y ∷ A


trivially-small : ∀ {i} (X : Type i) → Small i X
trivially-small X = X , ide X

trivially-locally-small : ∀ {i} (X : Type i) → LocallySmall i X
trivially-locally-small X x x' = trivially-small (x == x')


