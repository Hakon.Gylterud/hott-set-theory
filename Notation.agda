{-# OPTIONS --without-K #-}
module Notation where

open import lib.Base

{- Some convenient notation. -}

¦_¦ : ∀ {i j} {B : Type i → Set j} → Σ (Type i) B → Type i
¦_¦ = fst

infix 150 _〈_〉

_〈_〉 : ∀ {i j k} {A : Type i} {B : A → Type j} {C : ((a : A) → B a) → Type k}
    → Σ ((a : A) → B a) C
    → (a : A) → B a
f 〈 a 〉 = fst f a



pick : ∀ {i j} {B : Type i → Set j} → ( x : Σ (Type i) B) → B ¦ x ¦
pick = snd 
