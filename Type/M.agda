module Type.M where

open import lib.Base
open import lib.NType
open import Notation

module _ {i j} (S : Type i) (P : S → Type j) where
    module _ (M : Type (lmax i j))
             (desup : M → Σ S (λ s → P s → M)) where
        Final : ∀ k → Type (lmax (lsucc k) (lmax i j))
        Final k = ∀ (X : Type k) (f : X → Σ S (λ s → P s → X))
                             → is-contr (Σ (X → M)
                            (λ r → ∀ x → desup (r x) == (fst (f x) , λ p → r (snd (f x) p))))
