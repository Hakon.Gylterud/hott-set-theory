{-# OPTIONS --without-K #-}
{-
% Fibrewise equivalence lemma
% Håkon Robbestad Gylterud

This module gives an equivalence between "slice functions" and fiberwise
functions.

-}



module Function.Fiberwise where

open import lib.Base
open import lib.Equivalences
open import lib.Equivalences2
open import lib.Funext
open import lib.types.Sigma
open import lib.types.Pi
open import lib.PathGroupoid
open import lib.NType
open import Propositions.Equivalences


-- The following lemma says how fibers behave over paths:

hfiber-transport : ∀ {i j} {A : Type i} {B : Type j} → (f : A → B)
                → {b b' : B} → (h : b == b')
                → ∀ a e → (a , e) == (a , e ∙ h) [ hfiber f ↓ h ]
hfiber-transport f idp a idp = idp



{- Dependent paths -}

ap-idp : ∀ {i j} {A : Type i} {B : Type j}
     → (f : A → B)
     → {a a' : A}
     → (p : a == a')
     → ap f p == idp [ (λ v → f v == f a') ↓ p ]
ap-idp f idp = idp



-- The following lemma is a bit of a brute force path calculation:

ap-idp' : ∀ {i j} {A : Type i} {B : Type j}
     → (f r : A → B)
     → (σ : ∀ a → f a == r a)
     → {a a' : A}
     → (p : a' == a)
     → (! (σ a') ∙ ap f p) ∙' (σ a) == idp [ (λ v → r v == r a) ↓ p ]
ap-idp' f r σ {a = a} idp = ap (λ x → x ∙' σ a)
                               (∙-unit-r (! (σ a))) ∙ (!-inv'-l (σ a))


-- The following lemma explains how `ap` works with compositions.

ap-comp : ∀ {i j k} {A : Type i} {B : Type j} {C : Type k}
        → (f : A → B) (g : B → C)
        → {a a' : A} (p : a == a')
        → ap g (ap f p) == ap (λ a → g (f a)) p
ap-comp f g idp = idp


ap2d : ∀ {i j k} {A : Type i} {B : A → Type j} {C : Type k}
     → (F : ∀ a → B a → C)
     → {a a' : A} {b : B a} {b' : B a'}
     → (p : a == a') (q : b == b' [ B ↓ p ])
     → F a b == F a' b'
ap2d F idp idp = idp


{- Algebraic lemmas -}

prod-commute : ∀ {i j k} {A : Type i}
             → (B : A → Type j) (C : {a : A} → B a → Type k)
             → (Σ (Π A B) (λ f → Π A (C ∘ f))) ≃ (Π A (λ x → Σ (B x) C))
prod-commute {A = A} B C = there , is-eq there back there-back back-there where
   there : (Σ (Π A B) (λ f → Π A (C ∘ f))) → (Π A (λ x → Σ (B x) C))
   there (f , s) x = (f x , s x)
   back : (Π A (λ x → Σ (B x) C)) → (Σ (Π A B) (λ f → Π A (C ∘ f)))
   back  F = (λ x → fst (F x)) , (λ x → snd (F x))
   there-back : ∀ F → there (back F) == F
   there-back F = idp
   back-there : ∀ fs → back (there fs) == fs
   back-there fs = idp


{- Fibrewise functions -}

-- Here comes the advertised equivalence:


module _ {i j k} {A : Type i} {B : Type j} {C : Type k}
         (f : A → C) (g : B → C) where
  Over : Type (lmax (lmax i j) k)
  Over = (Σ (A → B) (λ α → ∀ x → g (α x) == f x))

  Fibrewise : Type (lmax (lmax i j) k)
  Fibrewise = (x : C) → hfiber f x → hfiber g x

  over-to-fiberwise : Over → Fibrewise
  over-to-fiberwise (α , σ) c (a , p) = (α a , σ a ∙' p)

  fiberwise-to-over : Fibrewise → Over
  fiberwise-to-over F = (α , σ) where
     α : A → B
     α a = fst (F (f a) (a , idp))
     σ : (a : A) → g (α a) == f a
     σ a = snd (F (f a) (a , idp))

  fiberwise : Over ≃ Fibrewise
  fiberwise = over-to-fiberwise , is-eq _ fiberwise-to-over φ ψ where
     φ : ∀ F → over-to-fiberwise (fiberwise-to-over F) == F
     φ F = λ= (λ c → (λ= (w c))) where
        w : ∀ c ap → over-to-fiberwise (fiberwise-to-over F) c ap == F c ap
        w .(f a) (a , idp) = idp
     ψ : ∀ ασ → fiberwise-to-over (over-to-fiberwise ασ) == ασ
     ψ (α , σ) = idp



{- Fibrewise equivalences

Now we should show that [fiberwise] preserves equivalences.
The proofs here involve some ugly path computations.

-}


module _ {i j k} {A : Type i} {B : Type j} {C : Type k}
         (f : A → C) (g : B → C) where
  fiberwise-equiv₀ : ∀ ασ → is-equiv (apply ασ)
                   → (∀ c → is-equiv (apply (fiberwise f g) ασ c))
  fiberwise-equiv₀ (α , σ) e c = is-eq (F c) (F' c) (F-F' c) (F'-F c) where
        α' = is-equiv.g e
        σ' : ∀ b → f (α' b) == g b
        σ' b = ! (σ (α' b)) ∙ (ap g (is-equiv.f-g e b))
        F  = apply (fiberwise f g) (α , σ)
        F' = apply (fiberwise g f) (α' , σ')
        F-F' : ∀ c bp → F c (F' c bp) == bp
        F-F' .(g b) (b , idp) = pair= (is-equiv.f-g e b)
                                      (    use-∙'=∙
                                       ∙ᵈ (use-∙-assoc
                                       ∙ᵈ (use-!-inv-r
                                       ∙ᵈ ap-idp g (is-equiv.f-g e b)))) where
            use-∙'=∙ : (σ (is-equiv.g e b)
                     ∙' ! (σ (is-equiv.g e b)) 
                     ∙ ap g (is-equiv.f-g e b)) == 
                            (σ (is-equiv.g e b)
                             ∙ ! (σ (is-equiv.g e b))
                             ∙ ap g (is-equiv.f-g e b))
            use-∙'=∙ = ∙'=∙ (σ (is-equiv.g e b) )
                            (! (σ (is-equiv.g e b)) ∙ ap g (is-equiv.f-g e b))
            use-∙-assoc : σ (is-equiv.g e b)
                        ∙ ! (σ (is-equiv.g e b))
                        ∙ ap g (is-equiv.f-g e b) ==
                               (σ (is-equiv.g e b)
                                ∙ ! (σ (is-equiv.g e b)))
                                ∙ ap g (is-equiv.f-g e b)
            use-∙-assoc = ! (∙-assoc (σ (is-equiv.g e b))
                                        (! (σ (is-equiv.g e b)))
                                        (ap g (is-equiv.f-g e b)))
            use-!-inv-r : (σ (is-equiv.g e b)
                        ∙ ! (σ (is-equiv.g e b)))
                        ∙ ap g (is-equiv.f-g e b) == ap g (is-equiv.f-g e b)
            use-!-inv-r = ap (λ x → x ∙ ap g (is-equiv.f-g e b))
                             (!-inv-r (σ (is-equiv.g e b)))
        F'-F : ∀ c ap → F' c (F c ap) == ap
        F'-F .(f a) (a , idp) = 
            pair= (is-equiv.g-f e a)
                  (ap (λ x → ((! (σ (is-equiv.g e (α a))) ∙ ap g x) ∙' σ a))
                      (! (is-equiv.adj e a))
                       ∙ᵈ (ap (λ x → (((! (σ (is-equiv.g e (α a))) ∙ x) ∙' σ a)))
                              (ap-comp α g (is-equiv.g-f e a))
                           ∙ᵈ (ap-idp' (λ a → g (α a)) f σ (is-equiv.g-f e a))))

  fiberwise-equiv₁ : ∀ F → (∀ c → is-equiv (F c))
                   → is-equiv (apply (fiberwise-to-over f g F))
  fiberwise-equiv₁ F ε = is-eq α α' α-α' α'-α where
      F' = λ c bp → is-equiv.g (ε c) bp
      α = apply (fiberwise-to-over f g F)
      α' = apply (fiberwise-to-over g f F')
      α-α' : ∀ b → α (α' b) == b
      α-α' b = ap2d (λ c ap → fst (F c ap)) p (hfiber-transport f p a idp)
               ∙ (ap fst (is-equiv.f-g (ε c) x)) where
                    c = g b
                    x = (b , idp)
                    a = α' b
                    p : f a == c
                    p = (snd (F' c x))
      α'-α : ∀ a → α' (α a) == a
      α'-α a = ap2d (λ c bp → fst (F' c bp)) p (hfiber-transport g p b idp)
               ∙ (ap fst (is-equiv.g-f (ε c) x)) where
                    c = f a
                    x = (a , idp)
                    b = α a
                    p : g b == c
                    p = (snd (F c x))

  fiberwise-equiv : ∀ ασ → is-equiv (apply ασ)
                         ↔ (∀ c → is-equiv (apply (fiberwise f g) ασ c))
  fiberwise-equiv ασ
     = fiberwise-equiv₀ ασ ,
       λ ε → transport (λ ασ → is-equiv (apply ασ))
                      (<–-inv-l (fiberwise f g) ασ)
                      (fiberwise-equiv₁ _ ε)

-- Now we stitch together everything to the main result:

  ≃-Over : Type (lmax (lmax i j) k)
  ≃-Over = (Σ (A ≃ B) (λ α → ∀ x → g (apply α x) == f x))

  ≃-Fibrewise : Type (lmax (lmax i j) k)
  ≃-Fibrewise = (x : C) → hfiber f x ≃ hfiber g x


  Over-Fibrewise-≃ : ≃-Over ≃ ≃-Fibrewise
  Over-Fibrewise-≃ = (fiberwise-eq) ∘e (over-fiberwise) ∘e (over-equiv) where
     over-equiv : ≃-Over ≃ Σ (Over f g) (is-equiv ∘ apply)
     over-equiv = (Σ₂-×-comm) ⁻¹
     over-fiberwise : Σ (Over f g) (is-equiv ∘ apply)
                    ≃ Σ (Fibrewise f g) (λ F → (∀ c → is-equiv (F c)) )
     over-fiberwise = restricted-equiv (fiberwise f g)
                                       (is-equiv-is-prop ∘ apply)
                                       (Π-level ∘ (_∘_ is-equiv-is-prop))
                                       fiberwise-equiv₀ fiberwise-equiv₁
     fiberwise-eq : Σ (Fibrewise f g) (λ F → (∀ c → is-equiv (F c)) )
                    ≃ ≃-Fibrewise
     fiberwise-eq = (prod-commute _ _)

{-  Another useful lemma

This one is from the HoTT book -}

collect-fibers : ∀ {i j} {A : Type i} {B : Type j}
                 → (f : A → B) → A ≃ (Σ B (hfiber f))
collect-fibers {A = A} f = there , is-eq _ back ω (λ a → idp) where
   there = λ a → (f a , a , idp)
   back : (Σ _ (hfiber f)) → A
   back p = fst (snd p)
   ω : ∀ bap → there (back bap) == bap
   ω (.(f a) , a , idp) = idp

