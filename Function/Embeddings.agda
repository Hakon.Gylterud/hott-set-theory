{-# OPTIONS --without-K #-}
module Function.Embeddings where

open import lib.Base
open import lib.types.Sigma
open import lib.types.Paths
open import lib.types.Pi
open import lib.Equivalences
open import lib.Equivalences2
open import lib.PathGroupoid
open import lib.Funext
open import lib.NType
open import lib.NType2
open import Propositions.Equivalences

open import Notation

{- Definition and alternate formulation -}

is-embedding : ∀ {i j} {Domain : Type i} {Codomain : Type j}
             → (Domain → Codomain) → Type (lmax i j)
is-embedding f = ∀ x → is-prop (hfiber f x)

is-embedding-is-prop : ∀ {i j} {Domain : Type i} {Codomain : Type j}
             → (f : Domain → Codomain)
             → is-prop (is-embedding f)
is-embedding-is-prop f = Π-level (λ _ → is-prop-is-prop)

_↪_ :  ∀ {i j} → Type i → Type j → Type (lmax i j)
A ↪ B = Σ (A → B) is-embedding

{- The composition of two embeddings is an embedding -}

hfiber-∘≃ : ∀ {i j k} {A : Type i} {B : Type j} {C : Type k}
                  → (f : A → B)
                  → (g : B → C)
                  → (c : C)
                  → hfiber (g ∘ f) c ≃ Σ (hfiber g c) (λ u → hfiber f (fst u))
hfiber-∘≃ {A = A} {B = B} f g c
                     = (((Σ-assoc ⁻¹
                       ∘e equiv-Σ-snd (λ b → ×-comm))
                       ∘e equiv-Σ-snd (λ b → Σ-assoc ⁻¹))
                       ∘e Σ₁-×-comm)
                       ∘e eqv where

                         back-there : (x : Σ A (λ a → Σ B (λ b₁ → (f a == b₁) × (g b₁ == c))))
                                    → fst x , f (fst x) , idp ,
                                      ap g (fst (snd (snd x))) ∙ snd (snd (snd x)) == x
                         back-there (a , .(f a) , idp , q) = idp

                         eqv : Σ A (λ a → g (f a) == c)
                             ≃ Σ A (λ a → Σ B (λ b → (f a == b) × (g b == c)))
                         eqv = equiv (λ (a , p) → a , ((f a) , (idp , p)))
                                     (λ (a , (b , (p , q))) → a , (ap g p ∙ q))
                                     (back-there)
                                     λ _ → idp

emb-comp : ∀ {i j k} {A : Type i} {B : Type j} {C : Type k}
         → A ↪ B
         → B ↪ C
         → A ↪ C
emb-comp f g = (λ a → g 〈 f 〈 a 〉 〉) ,
               λ c → equiv-preserves-level
                       (hfiber-∘≃ (fst f) (fst g) c ⁻¹)
                       (Σ-level (snd g c) (λ u → snd f (fst u)))

syntax emb-comp f g = f ∘emb g

{- The product of two embeddings is an embedding -}

=×-eqv : ∀ {i j} {A : Type i} {B : Type j}
       → {x y : A × B}
       → (x == y) ≃ ((fst x == fst y) × (snd x == snd y))
=×-eqv {x = x} {y = y}
          = equiv there
                  back
                  there-back
                  back-there where

                    there : (x == y) → (fst x == fst y) × (snd x == snd y)
                    there idp = (idp , idp)

                    back : ((fst x == fst y) × (snd x == snd y)) → (x == y)
                    back (idp , idp) = idp

                    there-back : (u : (fst x == fst y) × (snd x == snd y))
                               → there (back u) == u
                    there-back (idp , idp) = idp

                    back-there : (u : x == y) → back (there u) == u
                    back-there idp = idp

emb-prod : ∀ {i j k l} {A : Type i} {B : Type j} {X : Type k} {Y : Type l}
       → A ↪ X
       → B ↪ Y
       → A × B ↪ X × Y
emb-prod f g = (λ (a , b) → (f 〈 a 〉 , g 〈 b 〉)) ,
               λ (x , y) → equiv-preserves-level
                             ((equiv-Σ-snd (λ (a' , b') → =×-eqv ⁻¹))
                             ∘e Σ-assoc ⁻¹
                             ∘e equiv-Σ-snd (λ _ → Σ₁-×-comm) ⁻¹
                             ∘e Σ-assoc)
                             (Σ-level (snd f x) (λ _ → snd g y))

syntax emb-prod f g = f ×emb g

{- Show that if the fibers of a function f : A → B are propositions
   then for all a a' : A, the map (ap f a a') is an equivalence.

   Given a family of functions between two dependent types over the
   same domain, we construct 'Σ-fun', a function between the corresponding
   Σ-types, in the obvious way.
-}

Σ-fun : ∀ {i j k} {X : Type i} {P : X → Type j} {Q : X → Type k}
      → ((x : X) → P x → Q x)
      → Σ X P → Σ X Q
Σ-fun f (x , p) = (x , f x p)

{- Then we show that the fibers of 'Σ-fun' are equivalent to the fibers
   of the original family of functions. 
-}

Σ-fun-hfiber≃ : ∀ {i j k} {X : Type i} {P : X → Type j} {Q : X → Type k}
               → (f : (x : X) → P x → Q x)
               → (x : X) (q : Q x)
               → hfiber (f x) q ≃ hfiber (Σ-fun f) (x , q)
Σ-fun-hfiber≃ f x q = equiv (λ (p , r) → ((x , p) , (pair= idp r)))
                            back
                            back-there
                            there-back where

                              back : hfiber (Σ-fun f) (x , q) → hfiber (f x) q
                              back ((y , p) , idp) = (p , idp)

                              back-there : (b : hfiber (Σ-fun f) (x , q))
                                         → (x , fst (back b)) , ap (_,_ x) (snd (back b)) == b
                              back-there ((y , p) , idp) = idp

                              there-back : (a : hfiber (f x) q)
                                         → back ((x , fst a) , ap (_,_ x) (snd a)) == a
                              there-back (p , idp) = idp

{- Using the notion of contractible maps for equivalences, it thus
   follows that if 'Σ-fun' is an equivalence then all members of
   the family of functions are equivalences. 
-}

Σ-fun≃-hfiber≃ : ∀ {i j k} {X : Type i} {P : X → Type j} {Q : X → Type k}
                → (f : (x : X) → P x → Q x)
                → is-equiv (Σ-fun f)
                → (x : X) → is-equiv (f x)
Σ-fun≃-hfiber≃ f eqvf x = contr-map-is-equiv
                                  λ q → equiv-preserves-level
                                          (Σ-fun-hfiber≃ f x q ⁻¹)
                                          (equiv-is-contr-map eqvf (x , q))

{- Now, suppose that f is an embedding. Given a : A we construct
   Σ-fun (ap f a) : Σ A (λ x → a == x) → Σ A (λ x → f a == f x).
   Since both the domain and the codomain are contractible, it
   follows that Σ-fun (ap f a) is an equivalence. The desired result
   then follows by an application of 'Σ-fun≃-hfiber≃'.
-}

hfiber-prop-ap≃ : ∀ {i j} {A : Type i} {B : Type j}
                 → {f : A → B}
                 → is-embedding f
                 → (a a' : A) → is-equiv (ap f {a} {a'})
hfiber-prop-ap≃ {A = A} {f = f} fe a = Σ-fun≃-hfiber≃
                                       apf
                                       (contr-to-contr-is-equiv
                                         (Σ-fun apf)
                                         (pathfrom-is-contr a)
                                         contr) where

                                           apf : (x : A) → a == x → f a == f x
                                           apf x = ap f {a} {x}

                                           contr : is-contr (Σ A (λ z → f a == f z))
                                           contr = (inhab-prop-is-contr
                                                     (a , idp)
                                                     (equiv-preserves-level
                                                       (equiv-Σ-snd λ x → !-equiv)
                                                       (fe (f a))))

{- Show that if a function ap f is an equivalence then its fibers
   are propositions.

   This proof shows that if a fiber is inhabited, then
   it is contractible.
   The fiber over b is Σ A (λ a → f a  == b),
   so if we have an element (a , p) then we can
   replace b in the type by f a, and get:
   Σ A (λ a' → f a' == f a ), which is equivalent
   to Σ A (λ a' → a' == a ), which is contractible.
-}
ap≃-hfiber-prop : ∀ {i j} {A : Type i} {B : Type j}
                → (f : A → B)
                → (∀ x y → is-equiv (ap f {x} {y}))
                → is-embedding f
ap≃-hfiber-prop f r b
  = inhab-to-contr-is-prop
       λ {(a , p) →
         transport
           (λ b' → is-contr (Σ _ (λ a' → (f a' ) == b')))
           p
           (equiv-preserves-level
              (equiv-Σ-snd (λ a' → ( _ , r a' _)))
              (pathto-is-contr _)) }

{- If all members of a family of types are propositions then fst
   is an embedding.
-}

fst-embedding : ∀ {i j} {A : Type i} {B : A → Type j}
              → (∀ a → is-prop (B a))
              → is-embedding (fst :> (Σ A B → A))
fst-embedding p a = equiv-preserves-level (hfiber-fst a ⁻¹) (p a)

ι : ∀ {i j} {A : Type i} (B : A → hProp j)
  → Σ A (λ a → ¦ B a ¦) ↪ A
ι B = fst , fst-embedding (snd ∘ B)

{- If the codomain of an embedding has level S n then the domain has level S n -}

embedding-preserves-level : ∀ {i j} {n : ℕ₋₂} {A : Type i} {B : Type j}
                          → A ↪ B
                          → has-level (S n) B
                          → has-level (S n) A
embedding-preserves-level (f , fe) lv a a' = equiv-preserves-level
                                               ((ap f , hfiber-prop-ap≃ fe a a') ⁻¹)
                                               (lv (f a) (f a'))

{- Relationship between injective functions and embeddings -}

emb-to-inj : ∀ {i j} {A : Type i} {B : Type j}
           → {f : A → B}
           → is-embedding f
           → ∀ {x y} → f x == f y → x == y
emb-to-inj {f = f} isf {x} {y} = is-equiv.g (hfiber-prop-ap≃ isf x y)

inj-to-emb : ∀ {i j} {A : Type i} {B : Type j}
           → is-set B
           → {f : A → B}
           → (∀ {x y} → f x == f y → x == y)
           → is-embedding f
inj-to-emb {A = A} isB {f = f} inj b
           = all-paths-is-prop
               (λ (a , p) (a' , p') → prop-equal (λ _ → isB _ _)
                                                 (inj (p ∙ ! p')))
