{-# OPTIONS --without-K #-}
module Function.Surjections where

open import lib.Base
open import Notation
open import Propositions.Existensial

is-surjective : ∀ {i j} {Domain : Type i} {Codomain : Type j}
              → (Domain → Codomain) → Type (lmax i j)
is-surjective f = ∀ y → ∃ _ (λ x → f x == y)

_↠_ :  ∀ {i j} → Type i → Type j → Type (lmax i j)
A ↠ B = Σ (A → B) is-surjective

