{-# OPTIONS --without-K --rewriting #-}

{-
   This file postulates the existence of small images of
   small types into locally small types.
-}
module Function.ImageFactorisation where

open import lib.Base
open import Notation
open import Function.Embeddings
open import Function.Surjections
open import Smallness
open import Propositions.Existensial
open import lib.types.Truncation

module _ {i j}
         {Domain : Type i} {Codomain : Type j}
         (_ : LocallySmall i Codomain)
         (f : Domain → Codomain) where

  postulate Image : Type i

  postulate image-inclusion : Image ↪ Codomain

  postulate image-quotient : Domain ↠ Image
  
  postulate image-β : ∀ x → (image-inclusion 〈 image-quotient 〈 x 〉 〉) ↦ f x
  
  {-# REWRITE image-β #-}

  image-complete 
    : ∀ z → ∃ Domain (λ x → f x == image-inclusion 〈 z 〉)
  image-complete z 
      = ∃-elim _ _
               (λ _ → ∃-is-prop Domain (λ x → f x == image-inclusion 〈 z 〉))
               (λ (x , p) → [ x , ap (fst image-inclusion) p ] )
               (snd image-quotient z)
