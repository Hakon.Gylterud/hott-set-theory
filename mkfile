
MKSHELL=$PLAN9/bin/rc

AGDA=agda --without-K -i HoTT-Agda -i .


%.agdai:Q: %.agda
   $AGDA $stem.agda | ssam -e 'x/:[0-9]+,[0-9]+-[0-9]+/ x/-.*/ d' | ssam -e 's/:([0-9]+),([0-9]+)/:\1:\2/g'
   echo '
   ───────────────
   '
   /bin/echo -e ':m\n:quit' | $AGDA -I $stem.agda | ssam -ne 'x/Main>(.|\n)*/ p' | ssam -e 'x/Main. / d' | ssam -e 'x/^\?/ i/\n/'

HoTT-Agda/%.agdai:Q: HoTT-Agda/%.agda
    $AGDA HoTT-Agda/$stem.agda


clean:VQ:
  find . | grep '\.agdai$' | xargs /bin/rm -v



clean:QV:
  agdais=`{find . | grep '\.agdai$'}
  for (q in $agdais)
    rm -v $q || echo -n ''


all:QV: HoTT-Agda/lib/Basics.agdai
   echo 'Done.'
