module Propositions.Equivalences where

open import lib.Base
open import lib.NType
open import lib.NType2
open import lib.Equivalences
open import lib.types.Sigma
open import lib.types.Pi

-- Apply equivalences
-- (the notation in lib.Equivalences is annoying)

apply = fst

infix 4 _↔_

_↔_ : ∀ {i j} → Type i → Type j → Type (lmax i j)
_↔_ = \a b → (a → b) × (b → a)

↔-level : ∀ {i j} {n : ℕ₋₂} {A : Type i} {B : Type j}
  → (has-level n A → has-level n B → has-level n (A ↔ B))
↔-level Al Bl = ×-level (→-level Bl) (→-level Al)

is-prop-if-true : ∀ {i} {A : Type i} → (A → is-prop A) → is-prop A
is-prop-if-true p = \x → p x x

prop-≃-is-↔ : ∀ {i j} {A : Type i} {B : Type j}
            → is-prop A → is-prop B
            → (A ≃ B) ≃ (A ↔ B)
prop-≃-is-↔ p q = (\α → apply α , apply (α ⁻¹)) , is-eq _ φ κ δ where
   φ = \e → (fst e) , is-eq _ (snd e) (\b → prop-has-all-paths q _ _) (\a → prop-has-all-paths p _ _)
   κ = \e → prop-has-all-paths (↔-level p q) _ _
   δ = \e → prop-has-all-paths (≃-level p q) _ _


prop-equal : ∀ {i j} {A : Type i}
           → {P : A → Type j}
           → (∀ a → is-prop (P a))
           → {a a' : A} {p : P a} {p' : P a'}
           → (a == a')
           → (a , p) == (a' , p')
prop-equal q idp = pair= idp (prop-has-all-paths (q _) _ _)

has-all-paths-over : ∀ {i j} {A : Type i}
           → (B : A → Type j) → Type (lmax j i) 
has-all-paths-over {A = A} B
           = {a a' : A} (p : a == a')
           → (b : B a) (b' : B a')
           → b == b' [ B ↓ p ]

contr-has-all-paths-over : ∀ {i j} {A : Type i} 
                         → {B : A → Type j}
                         → Π A (is-contr ∘ B) → has-all-paths-over B
contr-has-all-paths-over c idp b b' = contr-has-all-paths (c _) b b'

Σ-contract : ∀ {i j} {A : Type i}
           → {P : A → Type j}
           → (∀ a → is-contr (P a))
           → Σ A P ≃ A
Σ-contract c = fst , is-eq _ (\a → (a , fst (c a))) φ ψ where
           φ = \a → idp
           ψ = \ap → pair= idp (contr-has-all-paths-over c _ _ _)


is-prop-has-contr-path-over : ∀ {i j} {A : Type i}
           → {B : A → Type j}
           → (∀ a → is-prop (B a))
           → {a a' : A} → (p : a == a')
           → (b : B a) (b' : B a')
           → is-contr (b == b' [ B ↓ p ])
is-prop-has-contr-path-over c idp = c _


prop-equal-≃ : ∀ {i j} {A : Type i}
           → {P : A → Type j}
           → (∀ a → is-prop (P a))
           → {a a' : A} (p : P a) (p' : P a')
           → (a == a') ≃ ((a , p) == (a' , p'))
prop-equal-≃ q p p' = =Σ-eqv (_ , p ) (_ , p') ∘e (Σ-contract (\p → is-prop-has-contr-path-over q p _ _) ⁻¹) --() ∘e (=Σ-eqv _ _)⁻¹


restricted-equiv : ∀ {i j k l} {A : Type i} {B : Type j}
                 → (α : A ≃ B)
                 → {P : A → Type k} {Q : B → Type l}
                 → (∀ a → is-prop (P a))
                 → (∀ b → is-prop (Q b))
                 → (∀ a → P a → Q (apply α a))
                 → (∀ b → Q b → P (apply (α ⁻¹) b))
                 → Σ A P ≃ Σ B Q
restricted-equiv α {P = P} {Q = Q} prop-P prop-Q ε δ
        = equiv-Σ α 
           (\p → apply ((prop-≃-is-↔ (prop-P _) (prop-Q _))⁻¹) 
                        (transport (\p' → P (apply (α ⁻¹) p) → Q p') 
                                   (<–-inv-r α p) (ε _) , δ _) )
