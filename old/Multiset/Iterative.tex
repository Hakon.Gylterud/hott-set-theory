\subsection{Multiset/Iterative.magda}
\begin{Verbatim}[fontsize=\footnotesize]
module Multiset.Iterative where

open import lib.Base
open import lib.Equivalences
open import lib.Funext
open import lib.Univalence
open import lib.types.Sigma
open import lib.types.Pi
open import lib.PathGroupoid
open import W.W
open import Function.Fiberwise
open import Propositions.Equivalences
\end{Verbatim}

We define an explicit decoding function for each Type i universe.

\begin{Verbatim}[fontsize=\footnotesize]
T : (i : ULevel) → Type i → Type i
T i A = A
\end{Verbatim}

\subsubsection{The type of iterative
multisets.}\label{the-type-of-iterative-multisets.}

\begin{Verbatim}[fontsize=\footnotesize]
M : (i : ULevel) → Type (lsucc i)
M i = W (T i)

index : ∀ {i} → M i → Set i
index (sup A _) = A

element : ∀ {i} → (x : M i) → index x → M i
element (sup A f) = f
\end{Verbatim}

\subsubsection{The natural equality on
M}\label{the-natural-equality-on-m}

\begin{Verbatim}[fontsize=\footnotesize]
Eq : ∀ {i} → M i → M i  → Set i
Eq (sup A f) (sup B g) = 
   Σ (A ≃ B) 
     (\α → ((x : A) → Eq (g (apply α x)) (f x)))
\end{Verbatim}

\subsubsection{Membership:}\label{membership}

x ∈ y is the set of occurences of x in y

\begin{Verbatim}[fontsize=\footnotesize]
_∈_ : ∀ {i} → M i → M i → Set i
x ∈ (sup A f) = Σ A (\i → Eq (f i) x)

_ε_ : ∀ {i} → M i → M i → Set (lsucc i)
x ε y = Σ (index y) (\i → (element y i) == x)

Eq-refl : ∀ {i} → {x : M i} → Eq x x
Eq-refl {_} {sup _ _} = ( ide _ , \x → Eq-refl)

Id-to-Eq : (i : ULevel) {x y : W (T i)} → x == y → Eq x y
Id-to-Eq i idp = Eq-refl
\end{Verbatim}

\subsubsection{A version of Eq which lives at a higher
ULevel}\label{a-version-of-eq-which-lives-at-a-higher-ulevel}

\begin{Verbatim}[fontsize=\footnotesize]
Eq' : ∀ {i} → M i → M i  → Set (lsucc i)
Eq' (sup A f) (sup B g) = 
   Σ (A ≃ B) 
     (\α → ((x : A) → Eq' (g (apply α x)) (f x)))
\end{Verbatim}

Changing the ULevel doesn't change anything:

\begin{Verbatim}[fontsize=\footnotesize]
Eq'-to-Eq : {i : ULevel} (x y : M i) → Eq' x y → Eq x y
Eq'-to-Eq (sup A f) (sup B g) (α , σ) = (α , \a → Eq'-to-Eq _ _ (σ _))

Eq'-is-Eq : {i : ULevel} (x y : M i) → Eq' x y ≃ Eq x y
Eq'-is-Eq (sup A f) (sup B g)
          = equiv-Σ-snd (\α → equiv-Π-r (\x → Eq'-is-Eq _ _))
\end{Verbatim}

\subsubsection{Id is Eq}\label{id-is-eq}

We can now prove that Id is equivalent to Eq'

\begin{Verbatim}[fontsize=\footnotesize]
Id-is-Eq' : (i : ULevel) (x y : M i) → (x == y) ≃ Eq' x y
Id-is-Eq' i (sup A f) (sup B g) = IH ∘e EXT ∘e UA ∘e WLEM where

   IH : Σ (A ≃ B) (\α → ((x : A) → (g (apply α x)) == (f x)))
      ≃ Eq' (sup A f) (sup B g)
   IH = equiv-Σ-snd (\α → equiv-Π (ide _)
                                  (\x → Id-is-Eq' i (g (apply α x)) (f x)))

   EXT : Σ (A ≃ B) (\α → (g ∘ (apply α) == f))
       ≃ Σ (A ≃ B) (\α → ((x : A) → (g (apply α x)) == (f x)))
   EXT = equiv-Σ-snd (\α → app=-equiv)

   lem0 : {A : Type i} → (ua (ide A)) == idp
   lem0 = ua-η idp
   lem = equiv-induction 
          (\α → transport (T i) (fst ua-equiv α) == apply α) 
          (\A → ap (\f → coe (ap (T i) f)) lem0)
   UA : (Σ (A == B) (\α → (g ∘ (transport (T i) α) == f)))
      ≃  Σ (A ≃ B)  (\α → (g ∘ (apply α) == f))
   UA = (equiv-Σ-snd (\α → coe-equiv (ap (\h → g ∘ h == f) (lem α)))) 
     ∘e (equiv-Σ-fst (\α → g ∘ (transport (T i) α) == f)(snd ua-equiv)) ⁻¹
   WLEM : ((sup A f) == (sup B g))
        ≃ Σ (A == B) (\α → (g ∘ (transport (T i) α) == f))
   WLEM = W-id-≃ (T i)
\end{Verbatim}

\ldots{}and thus that Id is equivalent to Eq.

\begin{Verbatim}[fontsize=\footnotesize]
Id≃Eq : (i : ULevel) (x y : M i) → (x == y) ≃ Eq x y
Id≃Eq = \i x y → Eq'-is-Eq x y ∘e Id-is-Eq' i x y
\end{Verbatim}

A useful consequence is that ε and ∈ are equivalent

\begin{Verbatim}[fontsize=\footnotesize]
ε≃∈ : ∀ {i} {x y : M i} → (x ε y) ≃ (x ∈ y)
ε≃∈ {y = sup A f} = equiv-Σ-snd (\i → Id≃Eq _ _ _)
\end{Verbatim}

\subsubsection{Extensional equality on
multisets}\label{extensional-equality-on-multisets}

\begin{Verbatim}[fontsize=\footnotesize]
ExtEq : ∀ {i} → (x y : M i) → Set (lsucc i)
ExtEq x y = ∀ z → (z ε x) ≃ (z ε y)

Extensionality : ∀ {i} → (x y : M i) → (x == y) ≃ ExtEq x y
Extensionality (sup A f) (sup B g)
    = Over-Fibrewise-≃ f g ∘e φ ∘e Id≃Eq _ _ _ where
      φ : Eq (sup A f) (sup B g) ≃ ≃-Over f g
      φ = equiv-Σ-snd (\α → equiv-Π (ide _) (\x → (Id≃Eq _ _ _)⁻¹))
\end{Verbatim}
