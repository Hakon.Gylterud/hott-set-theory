\subsection{Function/Fiberwise.magda}
\begin{Verbatim}[fontsize=\footnotesize]
{-# OPTIONS --without-K #-}

module Function.Fiberwise where
\end{Verbatim}

This module gives an equivalence between ``slice functions'' and
fibrewise functions.

\begin{Verbatim}[fontsize=\footnotesize]
open import lib.Base
open import lib.Equivalences
open import lib.Equivalences2
open import lib.Funext
open import lib.types.Sigma
open import lib.types.Pi
open import lib.PathGroupoid
open import lib.NType
open import Propositions.Equivalences
\end{Verbatim}

\subsubsection{The fibre of a function in a
point}\label{the-fibre-of-a-function-in-a-point}

\begin{Verbatim}[fontsize=\footnotesize]
fibre : ∀ {i j} {A : Type i} {B : Type j} → (f : A → B) → B → Type (lmax i j)
fibre {A = A} f b = Σ A (\a → f a == b)
\end{Verbatim}

The following lemma says how fibres behave over paths:

\begin{Verbatim}[fontsize=\footnotesize]
fibre-transport : ∀ {i j} {A : Type i} {B : Type j} → (f : A → B)
                → {b b' : B} → (h : b == b')
                → ∀ a e → (a , e) == (a , e ∙ h) [ fibre f ↓ h ]
fibre-transport f idp a idp = idp
\end{Verbatim}

\subsubsection{Dependent paths}\label{dependent-paths}

\begin{Verbatim}[fontsize=\footnotesize]
ap-idp : ∀ {i j} {A : Type i} {B : Type j}
     → (f : A → B)
     → {a a' : A}
     → (p : a == a')
     → ap f p == idp [ (\v → f v == f a') ↓ p ]
ap-idp f idp = idp
\end{Verbatim}

The following lemma is a bit of a brute force path calculation:

\begin{Verbatim}[fontsize=\footnotesize]
ap-idp' : ∀ {i j} {A : Type i} {B : Type j}
     → (f r : A → B)
     → (σ : ∀ a → f a == r a)
     → {a a' : A}
     → (p : a' == a)
     → (! (σ a') ∙ ap f p) ∙' (σ a) == idp [ (\v → r v == r a) ↓ p ]
ap-idp' f r σ {a = a} idp = ap (\x → x ∙' σ a)
                               (∙-unit-r (! (σ a))) ∙ (!-inv'-l (σ a))
\end{Verbatim}

The following lemma explains how \texttt{ap} works with compositions.

\begin{Verbatim}[fontsize=\footnotesize]
ap-comp : ∀ {i j k} {A : Type i} {B : Type j} {C : Type k}
        → (f : A → B) (g : B → C)
        → {a a' : A} (p : a == a')
        → ap g (ap f p) == ap (\a → g (f a)) p
ap-comp f g idp = idp
\end{Verbatim}

\begin{Verbatim}[fontsize=\footnotesize]
ap2d : ∀ {i j k} {A : Type i} {B : A → Type j} {C : Type k}
     → (F : ∀ a → B a → C)
     → {a a' : A} {b : B a} {b' : B a'}
     → (p : a == a') (q : b == b' [ B ↓ p ])
     → F a b == F a' b'
ap2d F idp idp = idp
\end{Verbatim}

\subsubsection{Algebraic lemmas}\label{algebraic-lemmas}

Two general lemmas from ∏ and ∑ algebra.

\begin{Verbatim}[fontsize=\footnotesize]
sum-commute : ∀ {i j k} {A : Type i} (B : A → Type j) (C : A → Type k)
            → Σ (Σ A B) (C ∘ fst) ≃ Σ (Σ A C) (B ∘ fst)
sum-commute {A = A} B C = there , is-eq there back there-back back-there where
   there : Σ (Σ A B) (C ∘ fst) → Σ (Σ A C) (B ∘ fst)
   there ((a , b) , c) = ((a , c ) , b)
   back : Σ (Σ A C) (B ∘ fst) → Σ (Σ A B) (C ∘ fst)
   back  ((a , c ) , b) = ((a , b) , c)
   there-back : ∀ acb → there (back acb) == acb
   there-back ((a , c) , b) = idp
   back-there : ∀ abc → back (there abc) == abc
   back-there ((a , b) , c) = idp

prod-commute : ∀ {i j k} {A : Type i}
             → (B : A → Type j) (C : {a : A} → B a → Type k)
             → (Σ (Π A B) (\f → Π A (C ∘ f))) ≃ (Π A (\x → Σ (B x) C))
prod-commute {A = A} B C = there , is-eq there back there-back back-there where
   there : (Σ (Π A B) (\f → Π A (C ∘ f))) → (Π A (\x → Σ (B x) C))
   there (f , s) x = (f x , s x)
   back : (Π A (\x → Σ (B x) C)) → (Σ (Π A B) (\f → Π A (C ∘ f)))
   back  F = (\x → fst (F x)) , (\x → snd (F x))
   there-back : ∀ F → there (back F) == F
   there-back F = idp
   back-there : ∀ fs → back (there fs) == fs
   back-there fs = idp
\end{Verbatim}

\subsubsection{Fibrewise functions}\label{fibrewise-functions}

Here comes the advertised equivalence:

\begin{Verbatim}[fontsize=\footnotesize]
module _ {i j k} {A : Type i} {B : Type j} {C : Type k}
         (f : A → C) (g : B → C) where
  Over : Type (lmax (lmax i j) k)
  Over = (Σ (A → B) (\α → ∀ x → g (α x) == f x))

  Fibrewise : Type (lmax (lmax i j) k)
  Fibrewise = (x : C) → fibre f x → fibre g x

  over-to-fibrewise : Over → Fibrewise
  over-to-fibrewise (α , σ) c (a , p) = (α a , σ a ∙' p)

  fibrewise-to-over : Fibrewise → Over
  fibrewise-to-over F = (α , σ) where
     α : A → B
     α a = fst (F (f a) (a , idp))
     σ : (a : A) → g (α a) == f a
     σ a = snd (F (f a) (a , idp))

  fibrewise : Over ≃ Fibrewise
  fibrewise = over-to-fibrewise , is-eq _ fibrewise-to-over φ ψ where
     φ : ∀ F → over-to-fibrewise (fibrewise-to-over F) == F
     φ F = λ= (\c → (λ= (w c))) where
        w : ∀ c ap → over-to-fibrewise (fibrewise-to-over F) c ap == F c ap
        w .(f a) (a , idp) = idp
     ψ : ∀ ασ → fibrewise-to-over (over-to-fibrewise ασ) == ασ
     ψ (α , σ) = idp
\end{Verbatim}

\subsubsection{Fibrewise equivalences}\label{fibrewise-equivalences}

Now we should show that {[}fibrewise{]} preserves equivalences. The
proofs here involve some ugly path computations.

\begin{Verbatim}[fontsize=\footnotesize]
module _ {i j k} {A : Type i} {B : Type j} {C : Type k}
         (f : A → C) (g : B → C) where
  fibrewise-equiv₀ : ∀ ασ → is-equiv (apply ασ)
                   → (∀ c → is-equiv (apply (fibrewise f g) ασ c))
  fibrewise-equiv₀ (α , σ) e c = is-eq (F c) (F' c) (F-F' c) (F'-F c) where
        α' = is-equiv.g e
        σ' : ∀ b → f (α' b) == g b
        σ' b = ! (σ (α' b)) ∙ (ap g (is-equiv.f-g e b))
        F  = apply (fibrewise f g) (α , σ)
        F' = apply (fibrewise g f) (α' , σ')
        F-F' : ∀ c bp → F c (F' c bp) == bp
        F-F' .(g b) (b , idp) = pair= (is-equiv.f-g e b)
                                      (    use-∙'=∙
                                       ∙ᵈ (use-∙-assoc
                                       ∙ᵈ (use-!-inv-r
                                       ∙ᵈ ap-idp g (is-equiv.f-g e b)))) where
            use-∙'=∙ : (σ (is-equiv.g e b)
                     ∙' ! (σ (is-equiv.g e b)) 
                     ∙ ap g (is-equiv.f-g e b)) == 
                            (σ (is-equiv.g e b)
                             ∙ ! (σ (is-equiv.g e b))
                             ∙ ap g (is-equiv.f-g e b))
            use-∙'=∙ = ∙'=∙ (σ (is-equiv.g e b) )
                            (! (σ (is-equiv.g e b)) ∙ ap g (is-equiv.f-g e b))
            use-∙-assoc : σ (is-equiv.g e b)
                        ∙ ! (σ (is-equiv.g e b))
                        ∙ ap g (is-equiv.f-g e b) ==
                               (σ (is-equiv.g e b)
                                ∙ ! (σ (is-equiv.g e b)))
                                ∙ ap g (is-equiv.f-g e b)
            use-∙-assoc = ! (∙-assoc (σ (is-equiv.g e b))
                                        (! (σ (is-equiv.g e b)))
                                        (ap g (is-equiv.f-g e b)))
            use-!-inv-r : (σ (is-equiv.g e b)
                        ∙ ! (σ (is-equiv.g e b)))
                        ∙ ap g (is-equiv.f-g e b) == ap g (is-equiv.f-g e b)
            use-!-inv-r = ap (\x → x ∙ ap g (is-equiv.f-g e b))
                             (!-inv-r (σ (is-equiv.g e b)))
        F'-F : ∀ c ap → F' c (F c ap) == ap
        F'-F .(f a) (a , idp) = 
            pair= (is-equiv.g-f e a)
                  (ap (\x → ((! (σ (is-equiv.g e (α a))) ∙ ap g x) ∙' σ a))
                      (! (is-equiv.adj e a))
                       ∙ᵈ (ap (\x → (((! (σ (is-equiv.g e (α a))) ∙ x) ∙' σ a)))
                              (ap-comp α g (is-equiv.g-f e a))
                           ∙ᵈ (ap-idp' (\a → g (α a)) f σ (is-equiv.g-f e a))))

  fibrewise-equiv₁ : ∀ F → (∀ c → is-equiv (F c))
                   → is-equiv (apply (fibrewise-to-over f g F))
  fibrewise-equiv₁ F ε = is-eq α α' α-α' α'-α where
      F' = \c bp → is-equiv.g (ε c) bp
      α = apply (fibrewise-to-over f g F)
      α' = apply (fibrewise-to-over g f F')
      α-α' : ∀ b → α (α' b) == b
      α-α' b = ap2d (\c ap → fst (F c ap)) p (fibre-transport f p a idp)
               ∙ (ap fst (is-equiv.f-g (ε c) x)) where
                    c = g b
                    x = (b , idp)
                    a = α' b
                    p : f a == c
                    p = (snd (F' c x))
      α'-α : ∀ a → α' (α a) == a
      α'-α a = ap2d (\c bp → fst (F' c bp)) p (fibre-transport g p b idp)
               ∙ (ap fst (is-equiv.g-f (ε c) x)) where
                    c = f a
                    x = (a , idp)
                    b = α a
                    p : g b == c
                    p = (snd (F c x))

  fibrewise-equiv : ∀ ασ → is-equiv (apply ασ)
                         ↔ (∀ c → is-equiv (apply (fibrewise f g) ασ c))
  fibrewise-equiv ασ
     = fibrewise-equiv₀ ασ ,
       \ε → transport (\ασ → is-equiv (apply ασ))
                      (<–-inv-l (fibrewise f g) ασ)
                      (fibrewise-equiv₁ _ ε)
\end{Verbatim}

Now we stitch together everything to the main result:

\begin{Verbatim}[fontsize=\footnotesize]
  ≃-Over : Type (lmax (lmax i j) k)
  ≃-Over = (Σ (A ≃ B) (\α → ∀ x → g (apply α x) == f x))

  ≃-Fibrewise : Type (lmax (lmax i j) k)
  ≃-Fibrewise = (x : C) → fibre f x ≃ fibre g x


  Over-Fibrewise-≃ : ≃-Over ≃ ≃-Fibrewise
  Over-Fibrewise-≃ = (fibrewise-eq) ∘e (over-fibrewise) ∘e (over-equiv) where
     over-equiv : ≃-Over ≃ Σ (Over f g) (is-equiv ∘ apply)
     over-equiv = (sum-commute _ _) ⁻¹
     over-fibrewise : Σ (Over f g) (is-equiv ∘ apply)
                    ≃ Σ (Fibrewise f g) (\F → (∀ c → is-equiv (F c)) )
     over-fibrewise = restricted-equiv (fibrewise f g)
                                       (is-equiv-is-prop ∘ apply)
                                       (Π-level ∘ (_∘_ is-equiv-is-prop))
                                       fibrewise-equiv₀ fibrewise-equiv₁
     fibrewise-eq : Σ (Fibrewise f g) (\F → (∀ c → is-equiv (F c)) )
                    ≃ ≃-Fibrewise
     fibrewise-eq = (prod-commute _ _)
\end{Verbatim}

\subsubsection{Another useful lemma}\label{another-useful-lemma}

This one is from the book

\begin{Verbatim}[fontsize=\footnotesize]
collect-fibres : ∀ {i j} {A : Type i} {B : Type j}
                 → (f : A → B) → A ≃ (Σ B (fibre f))
collect-fibres {A = A} f = there , is-eq _ back ω (\a → idp) where
   there = \a → (f a , a , idp)
   back : (Σ _ (fibre f)) → A
   back p = fst (snd p)
   ω : ∀ bap → there (back bap) == bap
   ω (.(f a) , a , idp) = idp
\end{Verbatim}
