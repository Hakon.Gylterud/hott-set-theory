\subsection{Sets/Iterative.magda}
This module defines and proves the basic properties of iterative sets.

\begin{Verbatim}[fontsize=\footnotesize]
{-# OPTIONS --without-K #-}

module Sets.Iterative where

open import lib.Base
open import lib.Equivalences
open import lib.Funext
open import lib.Univalence
open import lib.types.Sigma
open import lib.types.Pi
open import lib.PathGroupoid
open import lib.NType
open import lib.NType2
open import W.W
open import Function.Fiberwise
open import Propositions.Equivalences

import Multiset.Iterative as M
\end{Verbatim}

\subsubsection{Noation}\label{noation}

\begin{Verbatim}[fontsize=\footnotesize]
T = M.T

M = M.M
\end{Verbatim}

\subsubsection{Definition of iterative
sets}\label{definition-of-iterative-sets}

A multiset is an iterative set if elementhood is propositional and all
elements are iterative sets as well.

\begin{Verbatim}[fontsize=\footnotesize]
is-iterative-set : ∀ {i} → M i → Type (lsucc i)
is-iterative-set (sup a f) 
    = (i : T _ a) → (is-contr (Σ _ \(j : T _ a) → f j == f i)
    × is-iterative-set (f i))


iterative-set-is-prop : ∀ {i} → (x : M i) → is-prop (is-iterative-set x)
iterative-set-is-prop (sup a f) 
      = Π-level (\i → ×-level is-contr-is-prop (iterative-set-is-prop (f i)))
\end{Verbatim}

\subsubsection{The collection of iterative
sets}\label{the-collection-of-iterative-sets}

\begin{Verbatim}[fontsize=\footnotesize]
V : ∀ i → Type (lsucc i)
V i = Σ (M i) is-iterative-set
\end{Verbatim}

\subsubsection{Accessing the various parts of a
multiset.}\label{accessing-the-various-parts-of-a-multiset.}

\begin{Verbatim}[fontsize=\footnotesize]
underlying-M : ∀ {i} → V i → M i
underlying-M = fst


index : ∀ {i} → V i → Set i
index x = M.index (underlying-M x)

element : ∀ {i} → (x : V i) → index x → V i
element (sup a f , e) i = (f i , snd (e i))
\end{Verbatim}

The underlying multiset of an element is the elment of the underlying
multiset:

\begin{Verbatim}[fontsize=\footnotesize]
element-underlies : ∀ {i} → (x : V i) → (i : index x)
                  → M.element (underlying-M x) i == underlying-M (element x i)
element-underlies (sup a f , e) i = idp
\end{Verbatim}

\subsubsection{Two equivalent forms of
elementhood}\label{two-equivalent-forms-of-elementhood}

\begin{Verbatim}[fontsize=\footnotesize]
_∈_ : ∀ {i} → V i → V i → Type i
x ∈ y = underlying-M x M.∈ underlying-M y

_ε_ : ∀ {i} → V i → V i → Type (lsucc i)
x ε y = underlying-M x M.ε underlying-M y
\end{Verbatim}

Given an element of the index of a set, we get an actual element.

\begin{Verbatim}[fontsize=\footnotesize]
elements-ε : ∀ {i} → (x : V i) → (i : index x) → (element x i ε x)
elements-ε (sup A f , p) i = (i , idp)

itset-element-is-itset : ∀ {i} → (x : V i) → (z : M i)
                       → z M.ε (underlying-M  x) → is-iterative-set z
itset-element-is-itset (sup A f , i) z (a , p)
         = transport is-iterative-set p (snd (i a))

underlying-full-and-faitful : ∀ {i} → (x y : V i)
                            → (underlying-M x == underlying-M y) ≃ (x == y)
underlying-full-and-faitful (x , p) (y , p')
         = prop-equal-≃ iterative-set-is-prop p p'
\end{Verbatim}

\subsubsection{Elementhood in itsets is
propositional}\label{elementhood-in-itsets-is-propositional}

\begin{Verbatim}[fontsize=\footnotesize]
ε-is-prop : ∀ {i} → (x : M.M i) (y : V i) → is-prop (x M.ε underlying-M y)
ε-is-prop x (sup a f , p)
   = inhab-to-contr-is-prop (\e → transport (\x → is-contr (x M.ε (sup a f)))
                                            (snd e)
                                            (fst (p (fst e))))
\end{Verbatim}

\subsubsection{Extensionality for sets}\label{extensionality-for-sets}

\begin{Verbatim}[fontsize=\footnotesize]
ExtEq : ∀ {i} → V i → V i → Type (lsucc i)
ExtEq x y = ∀ z → z ε x ↔ z ε y

ExtEqM : ∀ {i} → V i → V i → Type (lsucc i) 
ExtEqM x y = ∀ z → z M.ε (underlying-M x) ↔ z M.ε (underlying-M y)

ExtEq-is-prop : ∀ {i} (x y : V i) → is-prop (ExtEq x y)
ExtEq-is-prop x y = Π-level (\z → ↔-level (ε-is-prop (underlying-M z) x)
                                          (ε-is-prop (underlying-M z) y))

ExtEqM-is-prop : ∀ {i} (x y : V i) → is-prop (ExtEqM x y)
ExtEqM-is-prop x y = Π-level (\z → ↔-level (ε-is-prop z x)
                                           (ε-is-prop z y))

ExtEqM≃ExtEq : ∀ {i} → (x y : V i) → ExtEqM x y ≃ ExtEq x y
ExtEqM≃ExtEq x y = apply (prop-≃-is-↔ (ExtEqM-is-prop x y)
                                      (ExtEq-is-prop x y) ⁻¹)
                         (φ , ψ) where
     φ : ExtEqM x y → ExtEq x y
     φ p = p ∘ underlying-M
     ψ : ExtEq x y → ExtEqM x y
     ψ f z = (\q → (fst (f (z , itset-element-is-itset x z q)) q))
           , (\q → snd (f (z , itset-element-is-itset y z q)) q)

Extensionality : ∀ {i} (x y : V i) → (x == y) ≃ ExtEq x y
Extensionality x y = (ExtEqM≃ExtEq x y)
                   ∘e φ
                   ∘e M.Extensionality _ _
                   ∘e underlying-full-and-faitful _ _ ⁻¹ where
    φ : M.ExtEq (underlying-M x) (underlying-M y) ≃ ExtEqM x y
    φ = (equiv-Π (ide _) (\z → prop-≃-is-↔ (ε-is-prop z x) (ε-is-prop z y) ))
\end{Verbatim}

Equality of itsets is propositional, thus the collection of all
iterative sets is a set:

\begin{Verbatim}[fontsize=\footnotesize]
==-is-prop : ∀ {i} (x y : V i) → is-prop (x == y)
==-is-prop x y = equiv-preserves-level (Extensionality x y ⁻¹)
                                       (ExtEq-is-prop x y)

V-is-set : ∀ {i} → is-set (V i)
V-is-set = ==-is-prop
\end{Verbatim}

We need equality to be small, so we define a small version

\begin{Verbatim}[fontsize=\footnotesize]
_=e_ : ∀ {i} → V i → V i → Type i
x =e y = M.Eq (underlying-M x) (underlying-M y)

=e-to-== : ∀ {i} (x y : V i) → (x =e y) ≃ (x == y)
=e-to-== x y = underlying-full-and-faitful x y ∘e (M.Id≃Eq _ _ _ ⁻¹)
\end{Verbatim}

\subsubsection{Constructing sets by small
injections}\label{constructing-sets-by-small-injections}

\begin{Verbatim}[fontsize=\footnotesize]
_#_ : ∀ {i} {A : Type i}
  → (f : A → V i) → (∀ a a' → (a == a') ≃ (f a == f a'))
  → V i
f # e = (sup _ (underlying-M ∘ f)
      , \a → (equiv-preserves-level
                    (equiv-Σ-snd (\a' → (underlying-full-and-faitful _ _ ⁻¹)
                                      ∘e e a' a))
                    (pathto-is-contr a) , snd (f a) ))


_-_#_ : ∀ {i} {A : Type i}
  → (is-set A)
  → (f : A → V i) → (∀ a a' → (f a == f a') → a == a')
  → V i
p - f # e = f # (\a a' → apply (prop-≃-is-↔ (p _ _)
                                            (V-is-set _ _) ⁻¹)
                                            (ap f , e a a'))
\end{Verbatim}

\subsubsection{The index of an iterative set is a
set}\label{the-index-of-an-iterative-set-is-a-set}

\begin{Verbatim}[fontsize=\footnotesize]
index-is-set : ∀ {i} → (x : V i) → is-set (index x)
index-is-set (sup a f , p)
     = equiv-preserves-level (collect-fibres (element (sup a f , p)) ⁻¹
                                ∘e (equiv-Σ-snd
                                     (\y → equiv-Σ-snd
                                       (\i → underlying-full-and-faitful _ _))))
                             (Σ-level
                                   V-is-set
                                   (\y → prop-is-set (ε-is-prop (underlying-M y)
                                                                (sup a f , p))))
\end{Verbatim}

\subsubsection{Eliminating set quotients to
propositions}\label{eliminating-set-quotients-to-propositions}

These lemma are general, but are put here in lack of a better place.

\begin{Verbatim}[fontsize=\footnotesize]
open import lib.types.SetQuotient

SetQuot-prop-elim : ∀ {i j k} {A : Type i} {R : A → A → Type j} 
                  → (P : SetQuotient R → Type k)
                  → Π _ (is-prop ∘ P) → (∀ a → P q[ a ])
                  → Π _ P
SetQuot-prop-elim P p q 
    = SetQuot-elim (prop-is-set ∘ p)
                   q
                   (\r → fst (is-prop-has-contr-path-over p _ _ _)) where

SetQuot-prop-elim2 : ∀ {i j k} {A : Type i} {R : A → A → Type j} 
                  → (P : SetQuotient R → SetQuotient R → Type k)
                  → (∀ a b → is-prop (P a b)) → (∀ a b → P q[ a ] q[ b ])
                  → (∀ a b → P a b)
SetQuot-prop-elim2 P p q = SetQuot-prop-elim 
           (\a → ∀ b → P a b) (\a → (Π-level (\b → p a b))) 
           \a → SetQuot-prop-elim (\ b → P q[ a ] b) (\b → p q[ a ] b ) 
             \b → q a b
\end{Verbatim}

\subsubsection{Quotient lemma}\label{quotient-lemma}

This lemma goes into constructing pairs and unions.

\begin{Verbatim}[fontsize=\footnotesize]
module _ {i} {A : Set i} (f : A → V i) where
  self-equaliser : Set i
  self-equaliser = SetQuotient (\a b → f a == f b)

  inject : self-equaliser → V i
  inject = SetQuot-rec (V-is-set {i}) f (\e → e)

  inject-inj : ∀ a b → inject a == inject b → a == b
  inject-inj = SetQuot-prop-elim2 (\a b → inject a == inject b → a == b)
                                  (\a b → Π-level (\e → SetQuotient-is-set a b))
                                   \a b → quot-rel
  
  quotient-set : V i
  quotient-set = SetQuotient-is-set - inject # inject-inj


  quotient-bound : ∀ {j} {P : V i → Type j}
                 → (Π _ (is-prop ∘ P))
                 → ((a : A) → P (f a))
                 → ∀ z → (z ε quotient-set) → P z
  quotient-bound {P = P} p r z (qa , e) 
          = transport P (apply (underlying-full-and-faitful _ _) e)
                        (SetQuot-prop-elim _ (\c → p (inject c)) r qa )
\end{Verbatim}

\subsubsection{Iterated quotients}\label{iterated-quotients}

\begin{Verbatim}[fontsize=\footnotesize]
iterated-quotient : ∀ {i} → M i → V i
iterated-quotient (sup a f) = quotient-set (\i → iterated-quotient (f i))
\end{Verbatim}

\subsubsection{Truncated Aczel equality}\label{truncated-aczel-equality}

The following relation gives an alternative way of constructing V as a
quotient of M rather than as a subtype.

\begin{Verbatim}[fontsize=\footnotesize]
open import Propositions.Equivalences
open import Propositions.Disjunction
open import Propositions.Existensial
open import lib.types.Truncation

truncated-aczel-equality : ∀ {i} (x y : M i) → Type i
truncated-aczel-equality (sup a f) (sup b g)
   = (∀ x → ∃ _ (\y → truncated-aczel-equality (f x) (g y)))
   × (∀ y → ∃ _ (\x → truncated-aczel-equality (f x) (g y)))

_≈_ = \{i} (x y : M i) → truncated-aczel-equality x y


≈-to-== : ∀ {i} (x y : M i) → (p : is-iterative-set x) (q : is-iterative-set y)
           → x ≈ y → x == y
≈-to-== {i} (sup a f) (sup b g) p q r
     = apply (((Extensionality x y) ∘e (underlying-full-and-faitful _ _)) ⁻¹)
        (\z → (\{(i , α) →
                      ∃-elim _ _ (\_ → ε-is-prop _ y)
                             ((transport (\w → w M.ε (sup b g) ) α)
                              ∘ (ih₁ (f i) (snd (p i))))
                             (fst r i) }) ,
              \{(j , β) → ∃-elim _ _ (\_ → ε-is-prop _ x)
                             ((transport (\w → w M.ε (sup a f) ) β)
                              ∘ (ih₀ (g j) (snd (q j))))
                             (snd r j) }) where
   ih₁ : ∀ w → (iw : is-iterative-set w)
       → Σ b (\j → w ≈ g j) → Σ b (\j → g j == w )
   ih₁ w iw (j , e) = (j , ! (≈-to-== w (g j) iw (snd (q j)) e))
   ih₀ : ∀ w → (iw : is-iterative-set w)
       → Σ a (\i → f i ≈ w) → Σ a (\i → f i == w)
   ih₀ w iw (i , e) = (i , ≈-to-== (f i) w (snd (p i)) iw e)
   x : V i
   x = sup a f , p
   y : V i
   y = sup b g , q


≈-to-iterated-quotient : ∀ {i} (x : M i)
                       → x ≈ (underlying-M (iterated-quotient x))
≈-to-iterated-quotient (sup a f) 
          = (\i → [ (q[ i ] , ≈-to-iterated-quotient (f i))  ])
          , (SetQuot-prop-elim (\j → _)
                               (\_ → ∃-is-prop _ _)
                               (\j → [( j , ≈-to-iterated-quotient (f j))]))
\end{Verbatim}
