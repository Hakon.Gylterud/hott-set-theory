\subsection{Sets/Axioms.magda}
This module proves various axioms of constructive set theory for our
model.

\begin{Verbatim}[fontsize=\footnotesize]
{-# OPTIONS --without-K #-}


module Sets.Axioms where

open import Sets.Iterative
open import lib.Base
open import lib.Equivalences
open import lib.NType
open import lib.PathGroupoid
open import lib.Funext

open import lib.types.Span
open import lib.types.SetQuotient
open import lib.types.Pi
open import lib.types.Sigma
open import lib.types.Truncation

open import Propositions.Equivalences
open import Propositions.Disjunction
open import Propositions.Existensial

open import Function.Fiberwise

import Multiset.Iterative as M

open import W.W

open W
\end{Verbatim}

\subsubsection{Misc}\label{misc}

\begin{Verbatim}[fontsize=\footnotesize]
data Nil (i : _) : Type i where

data One (i : _) : Type i where
  ∗ : One i

One-is-contr : ∀ {i} → is-contr (One i)
One-is-contr = ∗ , \{∗ → idp}

One+One-is-set : ∀ {i} → is-set (One i + One i)
One+One-is-set (left ∗) (right ∗) ()
One+One-is-set (right ∗) (left ∗) ()
One+One-is-set (left ∗) (left ∗) idp idp = idp , \{idp → idp}
One+One-is-set (right ∗) (right ∗) idp idp = idp , \{idp → idp}


⊙ : ∀ {i j} (A : Set j) → A → One i
⊙ A _ = ∗

Nil-elim : ∀ {i j} {A : Nil i → Type j} → Π _ A
Nil-elim ()
\end{Verbatim}

\subsubsection{Constructions underlying the
axioms}\label{constructions-underlying-the-axioms}

\begin{Verbatim}[fontsize=\footnotesize]
∅ : ∀ {i} → V i
∅ = (sup (Nil _) Nil-elim , Nil-elim)

singleton : ∀ {i} → V i → V i
singleton x = (contr-is-set One-is-contr) - (\_ → x) # \{∗ ∗ p → idp}

p : ∀ {i} → V i → V i → V i
p {i} x y = quotient-set φ where
   φ : One i + One i → V i
   φ (left _) = x
   φ (right _) = y

[z∈_¦_] : ∀{i} → V i → (Σ (V i → Type i) (\P → Π _ (is-prop ∘ P))) → V i
[z∈ (sup a f , e) ¦ ( P , p) ] 
    = sup (Σ _ (P ∘ element (sup a f , e))) (f ∘ fst) , 
      (\x → inhab-prop-is-contr (x , idp)
                                (equiv-preserves-level
                                     (sum-commute _ _)
                                     (Σ-level
                                         (contr-is-prop (fst (e (fst x))))
                                         (\i → p _))) , snd (e (fst x) ) )

⋃ : ∀ {i} → V i → V i
⋃ {i} x = quotient-set φ where
  φ : Σ (index x) (index ∘ element x) → V i
  φ (i , j) = element (element x i) j

G : ∀ {i} → (x y : V i) → (index x → index y) → V i
G x y f = quotient-set (element y ∘ f) where

γ : ∀ {i} → (x y : V i) → V i
γ x y = quotient-set (G x y)
\end{Verbatim}

\paragraph{Bounded Quantification}\label{bounded-quantification}

Beware! Has been known to cause troubles.

\begin{Verbatim}[fontsize=\footnotesize]
bonded : ∀ {i j} {P : V i → Type j}
        → (x : V i)
        → ((i : index x) → P (element x i))
        → ∀ z → (z ε x) → P z
bonded {P = P} (sup A f , _) p z (i , e) 
   = transport P (apply (underlying-full-and-faitful _ _) e) (p i)
\end{Verbatim}

\subsubsection{Proof of axioms}\label{proof-of-axioms}

\begin{Verbatim}[fontsize=\footnotesize]
PAIR : ∀ {i} → ∀ (x : V i) y → Σ _ (\u → ∀ z → z ε u ↔ (z == x) ∨ (z == y))
PAIR x y = ( p x y , \z → (φ z , ψ z)) where
   φ = quotient-bound _ (\_ → ∨-is-prop _ _)
                     \{(left _) → [ left idp ] ;
                       (right _) → [ right idp ] }
   ψ : ∀ z → (z == x) ∨ (z == y) → z ε p x y
   ψ z = ∨-elim' (\_ → (ε-is-prop (underlying-M z) (p x y))) 
                 (\p → (q[ left ∗  ] , ! (ap underlying-M p)))
                 (\p → (q[ right ∗ ] , ! (ap underlying-M p)))

RSEP : ∀ {i} → {P : V i → Type i} → (Π _ (is-prop ∘ P))
    → ∀ x → Σ _ \u → ∀ z → (z ε u ↔ P z × (z ε x))
RSEP {P = P} p (sup A f , e)
     = [z∈ (sup A f , e) ¦ (_ , p)] , \z → (φ z , ψ z) where
        φ : ∀ z
          → z ε [z∈ (sup A f , e) ¦ (P , p)]
          → P z × (z ε (sup A f , e))
        φ z ((a , e) , r) 
            = (transport P
                         (apply (underlying-full-and-faitful _ _) r)
                         e
               , (a , r))
        ψ : ∀ z
           → P z × (z ε (sup A f , e))
           → z ε [z∈ (sup A f , e) ¦ (P , p)]
        ψ z (e , (a , r))
            = ((a , transport P
                              (apply (underlying-full-and-faitful _ _) (! r))
                              e)
                  , r)


trsp2 : ∀ {i j k} {A : Type i} {B : A → Type j} (P : (a : A) → B a → Type k)
      → {a a' : A}
      → (p : a == a')
      → (b : B a)
      → P a b → P a' (transport B p b)
trsp2 P idp b p = p

UNION : ∀ {i} → ∀ (x : V i)
      → Σ _ \y → ∀ z → (z ε y ↔ ∃ _ \u → (z ε u) × (u ε x))
UNION (sup A f , p) = (⋃ (sup A f , p) , \z → ( φ z , ψ z)) where
   φ = quotient-bound 
         _
        (\qij → ∃-is-prop _ _) 
        \{(i , j) → [ element (sup A f , p) i 
                      , ((j , element-underlies _ _ ) , (i , idp)) ] }
   ψ : ∀ z → ∃ _ (λ u → (z ε u) × (u ε sup A f , p)) → z ε ⋃ (sup A f , p)
   ψ z = ∃-elim _ _
               (\x → ε-is-prop (underlying-M z) (⋃ (sup A f , p)))
               \{(u  , (j , uj=z) , (i , fi=u))
                    → (q[ i , transport M.index (! fi=u) j ]
                       , (! (element-underlies _ _))
                          ∙ trsp2 (\x i → M.element x i == underlying-M z)
                                  (! fi=u) j uj=z) }

SCOLL : ∀ {i j} → (P : V i → V i → Type j)
      → ∀ a → (∀ x → (x ε a) → Σ _ (\y → P x y))
      → Σ _ (\b → (∀ x → (x ε a) → Σ _ (\y → (y ε b) × (P x y )))
                × (∀ y → (y ε b) → ∃ _ (\x → (x ε a) × (P x y ))))
SCOLL P (sup A f , p) r = (b , φ , ψ) where
   a = (sup A f , p)
   b = quotient-set (\i → fst (r (element a i) (i , idp)))
   φ : ∀ x → (x ε a) → Σ _ (\y → (y ε b) × (P x y ))
   φ x (i , p) = ( y
                 , (q[ i ] , idp)
                 , transport (\x → P x y)
                             (apply (underlying-full-and-faitful _ _) p)
                             (snd (r (element a i) (i , idp))) ) where
           y = fst (r (element a i) (i , idp))
   ψ : ∀ y → (y ε b) → ∃ _ (\x → (x ε a) × (P x y ))
   ψ = quotient-bound _ (\qi → ∃-is-prop _ _)
                        (\i → [ (element a i)
                              , (i , idp)
                               , snd (r (element a i) (i , idp))])

SUBC : ∀ {i j} → (P : V i → V i → V i → Type j)
     → ∀ {a b}
     → Σ _ (\c → ∀ u → (∀ x → (x ε a) → Σ _ (\y → (y ε b) × P u x y))
                     → (Σ _ (\d → (d ε c) 
                                × (∀ x → (x ε a) 
                                       → ∃ _ (\y → (y ε d) × (P u x y )))
                                × (∀ y → (y ε d)
                                       → ∃ _ (\x → (x ε a) × (P u x y ))))))
SUBC P {sup A f , p} {sup B g , p'} = γ a b
                                    , \u F → (d u F
                                           , (di u F , idp)
                                           , φ u F , ψ u F) where
  a = (sup A f , p)
  b = (sup B g , p')
  d = \u F → G a b (\i → fst (fst (snd (F (element a i) (elements-ε a i)))))
  di = \u F → q[ (\i → fst (fst (snd (F (element a i) (elements-ε a i))))) ]
  φ : ∀ u (F : ∀ x
             → (x ε a)
             → Σ _ (\y → (y ε b) × P u x y)) x
    → (x ε a) 
    → ∃ _ (\y → (y ε d u F) × (P u x y ))
  φ u F (.(f i) , e) (i , idp)
    = [ (fst (F (element a i) (elements-ε a i)))
      , (q[ i ]
        , (snd (fst (snd (F (element a i) (elements-ε a i))))))
      , transport (\x → P u x (fst (F (element a i) (elements-ε a i))))
                  (apply (underlying-full-and-faitful _ _) idp)
                  (snd (snd (F (element a i) (elements-ε a i))))]
  ψ : ∀ u F y → (y ε d u F) → ∃ _ (\x → (x ε a) × (P u x y ))
  ψ u F = quotient-bound 
                    _
                    (\qi → ∃-is-prop _ _)
                    (\i → [ element a i
                          , (elements-ε a i)
                          , transport
                               (P u (element a i))
                               (! (apply
                                      (underlying-full-and-faitful _ _)
                                      (snd (fst (snd (F (element a i)
                                                        (elements-ε a i)))))))
                               (snd (snd (F (element a i) (elements-ε a i)))) ])


ε-IND : ∀ {i j} (P : V i → Type j)
      → (∀ x → (∀ y → (y ε x) → P y) → P x)
      → (∀ x → P x)
ε-IND P φ (sup A f , e) 
     = φ x (bonded x (\i → ε-IND P φ (f i , snd (e i)))) where
        x = sup A f , e
\end{Verbatim}
