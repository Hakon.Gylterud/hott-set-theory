module M.M where

open import lib.Base
open import lib.types.Nat
open import W.W

data One (i : _) : Type i where
  ∗ : One i

module _ {i j} (A : Type i) (B : A → Type j) where

  data Step : ℕ → Type (lmax i j) where
    hole : Step 0
    sup-step : ∀ {n} → (a : A) (f : B a → Step n) → Step (S n)

  stepwise : W B → (n : ℕ) → Step n
  stepwise x O = hole
  stepwise (sup a f) (S n) = sup-step a (\x → stepwise (f x) n)


  coherent-step : (n : ℕ) → (Step n) → (Step (S n)) → Type (lmax i j)
  coherent-step O x y = One _
  coherent-step (S n) (sup-step a f) (sup-step a' f')
        = Σ (a == a') (\p → (b : B a) → coherent-step _ (f b) (f' (transport B p b)))

  coherent : ((n : ℕ) → (Step n)) → Type (lmax i j)
  coherent α = ∀ n → coherent-step n (α n) (α (S n))

  stepwise-is-coherent : ∀ x → coherent (stepwise x)
  stepwise-is-coherent x O = ∗
  stepwise-is-coherent (sup a f) (S n) = idp , (\b → stepwise-is-coherent _ _)

  M = Σ _ coherent