
-- Author: Håkon Robbestad Gylterud

{-# OPTIONS --without-K #-}
module Logic.Heyting where

open import lib.Base
open import lib.types.Sigma
open import Propositions.Disjunction
open import Propositions.Existensial

data ⊥ i : Set i where

data ⊤ i : Set i where
  ∗ : ⊤ i

data Formula {i} (V : Set i) : Set i where
  ∈-form :  V → V → Formula V
  ≡-form :  V → V → Formula V
  ∧-form :  Formula V → Formula V → Formula V
  ∨-form :  Formula V → Formula V → Formula V
  →-form :  Formula V → Formula V → Formula V
  ⊥-form :  Formula V
  ∀-form :  Formula (V + ⊤ _) → Formula V
  ∃-form :  Formula (V + ⊤ _) → Formula V


τ : ∀ {i k} → (ℳ : Type i) (∈ : ℳ → ℳ → Type i) {V : Set k} 
  → Formula V → (V → ℳ) → Type i
τ ℳ ∈ (∈-form v v') φ = ∈ (φ v) (φ v')
τ ℳ ∈ (≡-form v v') φ = (φ v) == (φ v')
τ ℳ ∈ (∧-form A B) φ = (τ ℳ ∈ A φ) × (τ ℳ ∈ B φ)
τ ℳ ∈ (→-form A B) φ = (τ ℳ ∈ A φ) → (τ ℳ ∈ B φ)
τ ℳ ∈ (∨-form A B) φ = (τ ℳ ∈ A φ) ∨ (τ ℳ ∈ B φ)
τ ℳ ∈ (⊥-form) φ = ⊥ _
τ ℳ ∈ (∀-form A) φ = Π ℳ (\x → τ ℳ ∈ A λ{(left v) → φ v; (right _) → x})
τ ℳ ∈ (∃-form A) φ = ∃ ℳ (\x → τ ℳ ∈ A λ{(left v) → φ v; (right _) → x})


σ : ∀ {i k} → (ℳ : Type i) (∈ : ℳ → ℳ → Type i) {V : Set k} 
  → Formula V → (V → ℳ) → Type i
σ ℳ ∈ (∈-form v v') φ = ∈ (φ v) (φ v')
σ ℳ ∈ (≡-form v v') φ = (φ v) == (φ v')
σ ℳ ∈ (∧-form A B) φ = (σ ℳ ∈ A φ) × (σ ℳ ∈ B φ)
σ ℳ ∈ (→-form A B) φ = (σ ℳ ∈ A φ) → (σ ℳ ∈ B φ)
σ ℳ ∈ (∨-form A B) φ = (σ ℳ ∈ A φ) + (σ ℳ ∈ B φ)
σ ℳ ∈ (⊥-form) φ = ⊥ _
σ ℳ ∈ (∀-form A) φ = Π ℳ (\x → σ ℳ ∈ A λ{(left v) → φ v; (right _) → x})
σ ℳ ∈ (∃-form A) φ = Σ ℳ (\x → σ ℳ ∈ A λ{(left v) → φ v; (right _) → x})