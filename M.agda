module M where

open import lib.Base
open import lib.NType
open import Notation

module _ {i j}
         (S : Type i)
         (P : S → Type j) where

  Coalgebra : ∀ k → Type (lmax (lsucc k) (lmax i j))
  Coalgebra k = Σ (Type k) (λ Object →
                Σ (Object → S) (λ state →
                  (∀ o (p : P (state o)) → Object)))


  state : ∀ {k} → (A : Coalgebra k) → ¦ A ¦ → S
  state = fst ∘ snd

  view  : ∀ {k} → (A : Coalgebra k)
        → ∀ o (p : P (state A o)) → ¦ A ¦
  view = snd ∘ snd

  Hom : ∀ {k l}
      → Coalgebra k
      → Coalgebra l
      → Type (lmax k (lmax l (lmax i j)))
  Hom Domain Codomain
     = Σ (¦ Domain ¦ → ¦ Codomain ¦) (λ map →
       Σ (∀ x → state Codomain (map x)
             == state Domain x) (λ state-coh →
         (∀ x p
                → view Codomain (map x) p
                == map (view Domain x (transport P (state-coh x) p)))))
  map : ∀ {k l}
      → {Domain : Coalgebra k}
      → {Codomain : Coalgebra l}
      → Hom Domain Codomain
      → ¦ Domain ¦ → ¦ Codomain ¦
  map = fst

  postulate M : Coalgebra (lmax i j)

  postulate M-prop : ∀ {k}
                   → (A : Coalgebra k)
                   → is-contr (Hom A M)

  desup : ¦ M ¦ → Σ S (λ s → P s → ¦ M ¦)
  desup x = state M x , view M x

  corec : ∀ {k} {X : Type k}
        → (s : X → S)
        → (∀ o → (P (s o)) → X)
        → X → ¦ M ¦
  corec {X = X} s v = map {Domain = (X , s , v)} {Codomain = M}(fst (M-prop (X , s , v)))


