{-# OPTIONS --without-K #-}

module Power where

open import lib.Base
open import lib.types.Sigma
open import lib.types.Pi
open import lib.Equivalences
open import lib.PathFunctor
open import lib.PathGroupoid
open import lib.Funext
open import lib.NType
open import lib.NType2
open import lib.Univalence
open import Function.Fiberwise
open import Function.Embeddings

open import Notation
open import Smallness

{- Powersets -}

characteristic : ∀ {i j} (X : Type i)
               → Σ (Type j) (λ A → A ↪ X) → (X → hProp (lmax i j))
characteristic X (A , f) x = hfiber (fst f) x , snd f x

P : ∀ i {j} → Type j → Type (lmax (lsucc i) j)
P i A = Σ (Type i) (λ U → U ↪ A)

open import Function.ImageFactorisation

P-map : ∀ i {j k}
      → {X : Type j}
      → {Y : Type k}
      → LocallySmall i Y
      → (X → Y)
      → P i X → P i Y
P-map _ l f (A , i) = Image l (f ∘ fst i)
                    , image-inclusion l (f ∘ fst i)

T : ∀ i {j} → Type j → Type (lmax (lsucc i) j)
T i A = Σ (Type i) (λ U → U → A)

T-map : ∀ i {j k}
      → {X : Type j}
      → {Y : Type k}
      → (X → Y)
      → T i X → T i Y
T-map _ f (A , i) = A , (f ∘ i)

module _ {i j : ULevel} (X : Type j) where
  P-is-T-subtype : P i X ≃ Σ (T i X) (is-embedding ∘ snd)
  P-is-T-subtype = Σ-assoc ⁻¹
  
  T=-equiv-there : {A B : T i X}
                 → Σ (¦ A ¦ == ¦ B ¦)
                     (λ α → (snd B) ∘ coe α == (snd A))
                 → A == B
  T=-equiv-there {A , f } {.A , .f } (idp , idp) = idp

  T=-equiv-back : {A B : T i X}
                → A == B
                → Σ (¦ A ¦ == ¦ B ¦)
                    (λ α → (snd B) ∘ coe α == (snd A))
  T=-equiv-back {A , f} {.A , .f} idp = ( idp , idp)
  
  
  T=-equiv-back-there : {A B : T i X}
                      → ∀ p → T=-equiv-back {A} {B} (T=-equiv-there {A} {B} p) == p
  T=-equiv-back-there {A , f } {.A , .f } (idp , idp)
       = idp
       
  T=-equiv-there-back : {A B : T i X}
                      → ∀ p → T=-equiv-there {A} {B} (T=-equiv-back {A} {B} p) == p
  T=-equiv-there-back {A , f} {.A , .f} idp
       = idp
 
  
  T=-equiv : {A B : T i X}
           → (A == B) ≃ Σ (¦ A ¦ == ¦ B ¦)
                          (λ α → (snd B) ∘ coe α == (snd A))
  T=-equiv = equiv T=-equiv-back
                   T=-equiv-there
                   T=-equiv-back-there
                   T=-equiv-there-back
  

